package no.as.gold.nfc.communication;

import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.Serializable;
import java.util.UUID;

import no.as.gold.nfc.communication.Utils.Utils;
import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.nfc.communication.messages.ComHandlerMessage.StatusOptions;
import no.as.gold.simplemessenger.MessengerService;

/**
 * This class executes the writing to tags asynchronously and currently accepts strings to be written to the tag.
 */
public class NdefWriterTask  extends AsyncTask<Object, Void, ComHandlerMessage> {

    //region AsyncTask implementation
    @Override
    protected ComHandlerMessage doInBackground(Object... params) {
        int paramIndex = 0;
        UUID initiatorUUID = null;
        // Requires 2 arguments
        if(params != null && params.length < 2) {
            return new ComHandlerMessage(initiatorUUID, "Arguments missing.", StatusOptions.WRITE_FAILED);
        }

        // First argument must be a Tag
        if(!(params[paramIndex] instanceof Tag)) {
            return new ComHandlerMessage(initiatorUUID, "First argument must be an instance of type " + Tag.class.toString(), StatusOptions.WRITE_FAILED);
        }
        Tag tag = (Tag)params[paramIndex++];

        // Check to see if UUID is supplied
        if(params[paramIndex] instanceof UUID) {
            initiatorUUID = (UUID) params[paramIndex++];
        }

        // Payload is a string
        if(params[paramIndex] instanceof String) {
            String text = (String)params[paramIndex++];
            return writeToTag(initiatorUUID, Utils.NdefUtils.getTextAsNdefMessage(text), tag);
        }

        // Payload is an array of bytes
        if(params[paramIndex] instanceof byte[]) {
            // TODO: implement writing of bytes to tag
            return new ComHandlerMessage(initiatorUUID, "Writing of bytes to tag is not yet implemented", StatusOptions.WRITE_FAILED);
        }

        if(params[paramIndex] instanceof Serializable) {
            // TODO: implement writing of Serializable objects to tag
            return new ComHandlerMessage(initiatorUUID, "Writing of Serializable object to tag is not yet implemented", StatusOptions.WRITE_FAILED);
        }

        return new ComHandlerMessage(initiatorUUID, "Unknown object to send.", StatusOptions.WRITE_FAILED);
    }


    @Override
    /**
     * Simply transmits the message with the UI thread
     */
    protected void onPostExecute(ComHandlerMessage message) {
        MessengerService.Default.send(message);
    }
    //endregion

    //region Private methods

    /**
     * Writes a NdefMessage to a tag.
     *
     * @param senderUUID
     * @param message NdefMessage to write to tag
     * @param tag Tag to write to
     * @return Result of writing operation
     */
    private ComHandlerMessage writeToTag(UUID senderUUID, NdefMessage message, Tag tag) {
        int size = message.toByteArray().length;
        Ndef ndef = null;
        try {
            ndef = Ndef.get(tag);
            if(ndef != null) {
                if(!ndef.isWritable()) {
                    ndef.close();
                    return new ComHandlerMessage(senderUUID, "Tag is read-only", StatusOptions.WRITE_FAILED);
                }

                if(ndef.getMaxSize() < size) {
                    ndef.close();
                    return new ComHandlerMessage(senderUUID, "Not enough space on tag for message", StatusOptions.WRITE_FAILED);
                }

                Utils.SimpleTimer.startTimer(senderUUID.toString());
                ndef.connect();
                ndef.writeNdefMessage(message);
                ndef.close();
                double transferTime = Utils.SimpleTimer.getElapsedSeconds(senderUUID.toString());

                ComHandlerMessage msg = new ComHandlerMessage(senderUUID, "Successfully wrote to tag", StatusOptions.WRITE_SUCCESS);
                msg.TransferTimeInSeconds = transferTime;
                msg.TransferSizeInBytes = message.getByteArrayLength();
                return msg;
            }
        } catch(IOException e) {
            return new ComHandlerMessage(senderUUID, "Error communicating with tag: " + e.getMessage(), StatusOptions.WRITE_FAILED);
        } catch (FormatException e) {
            return new ComHandlerMessage(senderUUID, "Error deciphering the tag format: " + e.getMessage(), StatusOptions.WRITE_FAILED);
        } catch (Exception e) {
            return new ComHandlerMessage(senderUUID, "Error communicating with tag: " + e.getMessage(), StatusOptions.WRITE_FAILED);
        } finally { // Ensure that the connection is closed
            try {
                if(ndef != null)
                    ndef.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Utils.SimpleTimer.removeTimer(senderUUID.toString());
        }
        return new ComHandlerMessage(senderUUID, "Failed in writing to the tag", StatusOptions.WRITE_FAILED);
    }
    //endregion
}
