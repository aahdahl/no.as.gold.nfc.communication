package no.as.gold.nfc.communication.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;

/**
 * This class inspects ComHandlerMessages and reports their transfer speeds etc.
 * Created by aage on 25.11.13.
 */
public class Speedometer {

    private class SampleCollection {
        private int mMaxSize = 0;

        /**
         * Inner class for a sample
         */
        private class Sample {
            private double mTransferTime;
            private int mByteSize;
            public Sample(double transferTime, int byteSize) {
                mTransferTime = transferTime;
                mByteSize = byteSize;
            }

            public double getTransferTime() {
                return mTransferTime;
            }
            public int getTransferSize() {
                return mByteSize;
            }
        }

        List<Sample> mSampleList;

        // region constructors
        /**
         * Creates a continuous window of samples
         */
        public SampleCollection() {
            mSampleList = new ArrayList<Sample>();
        }

        /**
         * Creates a sliding window of latest samples.
         * @param maxSize Size of window.
         */
        public SampleCollection(int maxSize) {
            this();
            mMaxSize = maxSize;
        }

        // endregion

        // region public functions

        /**
         * Adds a sample to the collection
         * @param msg
         */
        public void addSample(ComHandlerMessage msg) {
            if(mMaxSize != 0 && mSampleList.size() == mMaxSize)
                removeOldest();
            mSampleList.add(new Sample(msg.TransferTimeInSeconds, msg.TransferSizeInBytes));
        }

        /**
         * Calculates the transfer speed.
         * @return
         */
        public double getTransferSpeed() {
            int transferredBytes = 0;
            double transferSeconds = 0;
            for(Sample sample : mSampleList) {
                transferredBytes += sample.getTransferSize();
                transferSeconds += sample.getTransferTime();
            }

            return transferSeconds == 0 ? 0 : transferredBytes / transferSeconds;

        }

        // endregion

        /**
         * Removes the oldest sample from the collection
         */
        private void removeOldest() {
            if(!mSampleList.isEmpty())
                mSampleList.remove(0);
        }
    }

    private int mHistorySize;
    private SampleCollection mReadSampleCollection;
    private SampleCollection mWrittenSampleCollection;
    private UUID mUuid;

    //region constructors

    /**
     * Monitors all incoming messages and calculates their transfer speed
     * @param historySize number of messages to base speed on
     */
    public Speedometer(int historySize) {
        mHistorySize = historySize;
        mReadSampleCollection = new SampleCollection(mHistorySize);
        mWrittenSampleCollection = new SampleCollection(mHistorySize);
    }

    /**
     * Monitors messages marked with the given {@link java.util.UUID}
     * @param uuidToMonitor {@link java.util.UUID} of sender to monitor
     * @param historySize number of messages to base speed on
     */
    public Speedometer(UUID uuidToMonitor, int historySize) {
        this(historySize);
        mUuid = uuidToMonitor;
    }
    //endregion

    //region public functions

    @Override
    /**
     * Returns the current transfer speed in bytes/seconds as a formatted string
     */
    public String toString() {
        return "Read speed: " + mReadSampleCollection.getTransferSpeed() + " bytes/sec.\nWrite speed: " + mWrittenSampleCollection.getTransferSpeed() + " bytes/sec.";
    }

    /**
     * Calculates the current read speed of the data communication
     * @return Read speed in bytes/second
     */
    public double getReadSpeed() {
        return mReadSampleCollection.getTransferSpeed();
    }

    /**
     * Calculates the current write speed of the data communication
     * @return Write speed in bytes/second
     */
    public double getWriteSpeed() {
        return mWrittenSampleCollection.getTransferSpeed();
    }

    /**
     * Starts the speedometer.
     */
    public void start() {
        registerListener();
    }

    /**
     * Stops the speedometer.
     */
    public void stop() {
        MessengerService.Default.UnRegister(this, ComHandlerMessage.class);
    }
    //endregion

    // region private functions
    private void registerListener() {
        MessengerService.Default.Register(this, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
            @Override
            public void handle(ComHandlerMessage msg) {
                // Assert that time has been set
                if(msg.TransferTimeInSeconds < 0)
                    return;

                // If only listening for transmissions related to a specific UUID
                if(mUuid != null && !mUuid.equals(msg.getSenderUUID())) return;

                switch(msg.Status) {
                    case WRITE_SUCCESS: mWrittenSampleCollection.addSample(msg); break;
                    case READ_SUCCESS: mReadSampleCollection.addSample(msg); break;
                }

                // Update listeners
            }
        });
    }
    // endregion
}
