package no.as.gold.nfc.communication.technologies;

import android.nfc.tech.TagTechnology;

import java.io.IOException;

/**
 * This is an IF that should be implemented as a wrapper class for all supported {@link android.nfc.tech.TagTechnology}.
 * The objective is to give the overlying layer a simplified interface that works with all supported {@link android.nfc.tech.TagTechnology}.
 * Created by Aage Dahl on 13.02.14.
 */
public interface ITagTechnologyWrapper {
    /**
     * Connects to the given technology to enable direct connection with tag.
     * @param tech Technology to connect to
     */
    void connect(TagTechnology tech) throws IOException;

    /**
     * Disconnects from the given technology to enable direct connection with tag.
     */
    void close() throws IOException;

    /**
     * Reads bytes from a given address on a {@link android.nfc.Tag}.
     * Prerequisite: Needs to call connect on a technology before running this.
     * @param length Number of bytes to be read.
     * @param address Address of starting byt to read from.
     * @return Bytes read from technology.
     * @throws java.lang.UnsupportedOperationException On unsupported technology.
     * @throws java.lang.NullPointerException if tech is null
     * @throws java.io.IOException if read operation fails
     */
    byte[] read(int length, int address) throws UnsupportedOperationException, NullPointerException, IOException;

    /**
     * Reads bytes from a given address on a {@link android.nfc.Tag}.
     * @param length Number of bytes to be read.
     * @param address Address of starting byt to read from.
     * @return Bytes read from technology.
     * @throws java.lang.UnsupportedOperationException On unsupported technology.
     * @throws java.lang.NullPointerException if tech is null
     * @throws java.io.IOException if read operation fails
     */
    byte[] read(TagTechnology tech, int length, int address) throws UnsupportedOperationException, NullPointerException, IOException;

    /**
     * Reads all bytes available on the {@link android.nfc.Tag}.
     * @return Bytes read from technology.
     * @throws java.lang.UnsupportedOperationException On unsupported technology.
     * @throws java.lang.NullPointerException if tech is null
     * @throws java.io.IOException if read operation fails
     */
    byte[] readAll(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException;

    /**
     * Reads all bytes available on the {@link android.nfc.Tag}.
     * Prerequisite: Needs to call connect on a technology before running this.
     * @return Bytes read from technology.
     * @throws java.lang.UnsupportedOperationException On unsupported technology.
     * @throws java.lang.NullPointerException if tech is null
     * @throws java.io.IOException if read operation fails
     */
    byte[] readAll() throws UnsupportedOperationException, NullPointerException, IOException;

    /**
     * Writes bytes to a given address on a {@link android.nfc.Tag}.
     *
     * @param bytes Bytes to be written.
     * @param address Address to write bytes to.
     * @throws java.lang.IndexOutOfBoundsException Thrown if operation writes outside of tag memory space.
     * @throws java.lang.UnsupportedOperationException On unsupported technology.
     * @throws java.lang.NullPointerException if tech is null
     * @throws java.io.IOException if write operation fails
     */
    void write(TagTechnology tech, byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException;

    /**
     * Writes bytes to a given address on a {@link android.nfc.Tag}.
     * Prerequisites: Need to call connect first
     *
     * @param bytes Bytes to be written.
     * @param address Address to write bytes to.
     * @throws java.lang.IndexOutOfBoundsException Thrown if operation writes outside of tag memory space.
     * @throws java.lang.UnsupportedOperationException On unsupported technology.
     * @throws java.lang.NullPointerException if tech is null
     * @throws java.io.IOException if write operation fails
     */
    void write(byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException;

    /**
     * Measures RTT of the tag. Will disconnect from tag upon completion.
     * @return time in ms for RTT
     * @throws java.lang.UnsupportedOperationException On unsupported technology.
     * @throws java.lang.NullPointerException if tech is null
     */
    double timeRTT(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException;

    /**
     * Gets the start address of contiguous memory available for reading and writing.
     * @return Start address for contiguous RW memory
     */
    int getMinContiguousRWAddress();

    /**
     * Gets the end address of contiguous memory available for reading and writing.
     * @return End address for contiguous RW memory
     */
    int getMaxContiguousRWAddress();


    /**
     * Transmits a command to the tag and observes the tags responce
     * @param tech Technology to transmit to
     * @param bytes Command to transmit
     * @return Responce from tag
     */
    byte[] transmitCommand(TagTechnology tech, byte[] bytes) throws IOException;
}
