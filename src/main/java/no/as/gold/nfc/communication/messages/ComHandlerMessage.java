package no.as.gold.nfc.communication.messages;


import android.nfc.NdefMessage;

import java.util.Arrays;
import java.util.UUID;
import java.util.zip.CRC32;

import no.as.gold.nfc.communication.Utils.Utils;
import no.as.gold.simplemessenger.messages.IMessage;

/**
 * This class informs interested parties that a read/write operation ended successfully
 * @author Aage Dahl
 *
 */
public class ComHandlerMessage implements IMessage {
    /**
     * Status on read/write operation
     * @author Aage Dahl
     *
     */
    public enum StatusOptions {
        READ_SUCCESS, WRITE_SUCCESS, READ_FAILED, WRITE_FAILED, CONNECTION_ERROR, TAG_MISSING, READ_PARAMETER_ERROR, WRITE_PARAMETER_ERROR, COMMAND_SUCCESS, COMMAND_FAILED
    }

    //region private members
    private java.util.UUID mSenderUUID;
    private String mMessage;
    private NdefMessage mNdefMessage;
    //endregion

    /**
     * Gets the UUID associated with this message. This will never be null!
     * @return UUID set for this message.
     */
    public UUID getSenderUUID() {
        return mSenderUUID == null ? UUID.randomUUID() : mSenderUUID;
    }
    private void setSenderUUID(UUID senderUUID) {
        mSenderUUID = senderUUID;
    }

    //region Public variables

    /**
     * Status of read/write operation
     */
    public StatusOptions Status;

    /**
     * Indicates read/write time in seconds.
     * Initially set to -1 to indicate that the value has not been set
     */
    public double TransferTimeInSeconds = -1;

    /**
     * Number of bytes transferred
     */
    public int TransferSizeInBytes;

    public byte[] Bytes;
    //endregion

    //region Constructors
    /**
     * Constructor for message with a write success
     * @param senderUUID Unique identifier of sender
     */
    public ComHandlerMessage(UUID senderUUID) {
        setSenderUUID(senderUUID);
        Status = StatusOptions.WRITE_SUCCESS;
    }

    /**
     * Constructor for a message with the payload of a read. Note that Status is set according to crc check.
     * @param senderUUID Unique identifier of sender
     * @param message what was read from the tag
     */
    public ComHandlerMessage(UUID senderUUID, NdefMessage message) {
        setSenderUUID(senderUUID);
        mNdefMessage = message;

        Status = Utils.NdefUtils.checkNdefCrc(message) ? StatusOptions.READ_SUCCESS : StatusOptions.READ_FAILED;
    }

    /**
     * Constructor for message that reports a read/write status
     * @param senderUUID Unique identifier of sender
     * @param status Status of operation
     */
    public ComHandlerMessage(UUID senderUUID, StatusOptions status)
    {
        setSenderUUID(senderUUID);
        Status = status;
    }

    /**
     * Constructor for a message with payload and a status
     * @param status        Status of operation
     * @param ndefMessage   NdefMessage read from the tag
     * @param senderUUID Unique identifier of sender
     */
    public ComHandlerMessage(UUID senderUUID, NdefMessage ndefMessage, StatusOptions status) {
        this(senderUUID, status);
        mNdefMessage = ndefMessage;
    }

    /**
     * String message with status
     * @param senderUUID Unique identifier of sender
     * @param message data read from tag
     * @param status status on reading/writing
     */
    public ComHandlerMessage(UUID senderUUID, String message, StatusOptions status) {
        this(senderUUID, status);
        mMessage = message;
    }

    /**
     * Raw byte responce from tag
     * @param senderUUID Unique identifier of sender
     * @param data Raw bytes read from tag
     * @param status status on reading/writing
     */
    public ComHandlerMessage(UUID senderUUID, byte[] data, StatusOptions status) {
        this(senderUUID, status);
        Bytes = data;
    }
    //endregion

    //region IMessage implementation

    /**
     * Gets the message describing the results of this ComHandlerMessage
     * @return Message describing this ComHandlerMessage
     */
    public String getMessage() {
        return mMessage == null ? "" : mMessage;
    }

    /**
     * Gets the information that was read from the tag
     * @return Information from the tag
     */
    public String getReadMessage() {
        return mNdefMessage == null ? Arrays.toString(Bytes) : Utils.NdefUtils.getNdefMessageAsText(mNdefMessage);
    }

    /**
     * Gets the CRC code of the received message if this is a read, null if not.
     * @return CRC code of the data contained in this message
     */
    public CRC32 getCRC() {
        return Utils.NdefUtils.getCRC(mNdefMessage);
    }

    //endregion

    //region Private methods
    //endregion

}
