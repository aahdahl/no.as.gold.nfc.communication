package no.as.gold.nfc.communication.technologies;

import android.nfc.tech.NfcF;
import android.nfc.tech.TagTechnology;

import org.apache.commons.lang3.ArrayUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.UUID;

import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.nfc.communication.Utils.Utils;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * NfcF is based on the JIS 6319-4 standard and is a Nfc forum type3 tag.
 * This class wraps around and implements functionality needed to communicate with a NfcF tag that supports FeliCa commands.
 * Improvement potential: it is assumed that the tag supports FeliCa commands but this is not asserted, assertion should be implemented in the future.
 * Created by Aage Dahl on 28.02.14.
 *
 * FeliCa description:
 * Memory is divided into Services, which are sub-divided into memory blocks which consist of 16 bytes each.
 * Memory blocks are addressed relative to the service they belong to (not directly)
 * FeliCa lite only has 2 services [ref http://www.sony.net/Products/felica/business/tech-support/data/fl_usmnl_1.2.pdf],
 * one for read and one for read/write access. R/W access is granted with the service code 0x0009 and R access is granted with the service code 0x000b
 * FeliCa standard supports 3 services; Random, Cyclic and Purse. FeliCa Lite only supports the Random service.
 *
 * Advantage of FeliCa
 * - Can read up to 4 blocks of data at a time
 * - Blocks read simultaneously do not need to be in contiguous memory
 */
public class NfcFFeliCaLiteSWrapper implements ITagTechnologyWrapper {

    // Size of a block is 16 according to NFC forum
    private final int BLOCK_SIZE = 16;

    private final byte CMD_WRITE_WO_ENCRYPTION = 0x08;      // Write without encryption command
    private final byte RES_WRITE_WO_ENCRYPTION = 0x09;      // Write without encryption responce
    private final byte CMD_READ_WO_ENCRYPTION = 0x06;       // Read without encryption command
    private final byte RES_READ_WO_ENCRYPTION_RES = 0x07;   // Read without encryption responce
    private final byte CMD_POLLING = 0x00;                  // Polling command
    private final byte RES_POLLING_RES = 0x01;              // Polling responce

    private final byte[] RW_SERVICE_CODE = {0x09, 0x00};    // Acquire read/write access service (little endian)
    private final byte[] R_SERVICE_CODE = {0x0B, 0x00};     // Acquire read access service (little endian)

    // Memory area that is r/w'able without interfering with system settings.
    private final int[] RW_BLOCKS = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D};
    public final static int MIN_CONTIGUOUS_RW_ADDR = 0;
    public final static int MAX_CONTIGUOUS_RW_ADDR = 0x0D*16;

    private NfcF mTech;
    private byte[] mId;

    //region ITagTechnologyWrapper implementation
    @Override
    public void connect(TagTechnology tech) throws IOException {
        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_NFC_F)) throw new UnsupportedOperationException("Tag does not support NfcF");

        mTech = (NfcF) tech;
        mTech.connect();
        mId = tech.getTag().getId();
    }

    @Override
    public void close() throws IOException {
        mTech.close();
    }

    @Override
    public byte[] read(int length, int address) throws UnsupportedOperationException, NullPointerException, IOException {
        // Note - can read 1 - 4 blocks at a time, need to handle all options
        int startBlock = address / BLOCK_SIZE;
        int startOffset = address % BLOCK_SIZE;
        int endBlock = (address + length - 1) / BLOCK_SIZE;

        // Assert that we are not trying to read outside of user space
        for(int i = startBlock; i <= endBlock; i++) {
            boolean found = false;
            for(int j = 0; j < RW_BLOCKS.length; j++) {
                if(RW_BLOCKS[j] == i) {
                    found = true;
                    break;
                }
            }
            if(!found) throw new IOException("Trying to read outside of user space!");
        }

        // Read blocks
        ByteBuffer buffer = ByteBuffer.allocate((1 + endBlock - startBlock) * BLOCK_SIZE);
        int i = startBlock;
        for(; i <= (endBlock - 3); i+=4) buffer.put(readBlocks(mTech, new int[] {i, i+1, i+2, i+3}, mId));
        for(; i <= (endBlock - 2); i+=3) buffer.put(readBlocks(mTech, new int[] {i, i+1, i+2}, mId));
        for(; i <= (endBlock - 1); i+=2) buffer.put(readBlocks(mTech, new int[] {i, i+1}, mId));
        for(; i <= (endBlock - 0); i+=1) buffer.put(readBlocks(mTech, new int[] {i}, mId));

        // Return sub array with requested data
        return ArrayUtils.subarray(buffer.array(), startOffset, startOffset + length);
    }

    @Override
    public byte[] read(TagTechnology tech, int length, int address) throws UnsupportedOperationException, NullPointerException, IOException {
        try {
            connect(tech);
            return read(length, address);
        } finally {
            close();
        }
    }

    @Override
    public byte[] readAll(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        try {
            connect(tech);
            return read(tech, getMaxContiguousRWAddress() - getMinContiguousRWAddress(), getMinContiguousRWAddress());
        } finally {
            close();
        }
    }

    @Override
    public byte[] readAll() throws UnsupportedOperationException, NullPointerException, IOException {
        return read(getMaxContiguousRWAddress() - getMinContiguousRWAddress(), getMinContiguousRWAddress());
    }

    @Override
    public void write(TagTechnology tech, byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        try {
            connect(tech);
            write(bytes, address);
        } finally {
            close();
        }
    }

    @Override
    public void write(byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        // Note - can read 1 - 4 blocks at a time, need to handle all options
        int startBlock = address / BLOCK_SIZE;
        int startOffset = address % BLOCK_SIZE;
        int endBlock = (address + bytes.length - 1) / BLOCK_SIZE;

        // Create padded byte array that is 16-bytes aligned
        byte[] data = new byte[(1 + endBlock - startBlock) * BLOCK_SIZE];
        int i = 0;
        for(; i < startOffset; i++) data[i] = 0;
        for(; i < startOffset + bytes.length; i++) data[i] = bytes[i - startOffset];
        for(; i < data.length; i++) data[i] = 0;


        // Assert that we are not trying to read outside of user space
        for(i = startBlock; i <= endBlock; i++) {
            boolean found = false;
            for(int j = 0; j < RW_BLOCKS.length; j++) {
                if(RW_BLOCKS[j] == i) {
                    found = true;
                    break;
                }
            }
            if(!found) throw new IOException("Trying to write outside of user space!");
        }

        // Write blocks
        i = startBlock;

        // felica lite-s rc-s966 can only write to one block simultaneously!
        //for(; i <= (endBlock - 3); i+=4) writeBlocks(mTech, new int[]{i, i + 1, i + 2, i + 3}, ArrayUtils.subarray(data, (i - startBlock) * BLOCK_SIZE, (i + 4 - startBlock) * BLOCK_SIZE), mId);
        //for(; i <= (endBlock - 2); i+=3) writeBlocks(mTech, new int[]{i, i + 1, i + 2}, ArrayUtils.subarray(data, (i - startBlock) * BLOCK_SIZE, (i + 3 - startBlock) * BLOCK_SIZE), mId);
        //for(; i <= (endBlock - 1); i+=2) writeBlocks(mTech, new int[]{i, i + 1}, ArrayUtils.subarray(data, (i - startBlock) * BLOCK_SIZE, (i + 2 - startBlock) * BLOCK_SIZE), mId);
        for(; i <= (endBlock - 0); i+=1) writeBlocks(mTech, new int[]{i}, ArrayUtils.subarray(data, (i - startBlock) * BLOCK_SIZE, (i + 1 - startBlock) * BLOCK_SIZE), mId);
    }

    @Override
    public double timeRTT(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_NFC_F)) throw new UnsupportedOperationException("Tag does not support NfcF");

        NfcF technology = (NfcF) tech;
        String uuid = UUID.randomUUID().toString();
        try {
            technology.connect();
            Utils.SimpleTimer.startTimer(uuid);
            byte[] res = technology.transceive(new byte[] {0x06, 0x00, (byte)0xff, (byte)0xff, 0x01, 0x00});
            return Utils.SimpleTimer.getElapsedSeconds(uuid);
        } finally {
            Utils.SimpleTimer.removeTimer(uuid);
            technology.close();
        }
    }

    @Override
    public int getMinContiguousRWAddress() {
        return MIN_CONTIGUOUS_RW_ADDR;
    }

    @Override
    public int getMaxContiguousRWAddress() {
        return MAX_CONTIGUOUS_RW_ADDR;
    }

    @Override
    public byte[] transmitCommand(TagTechnology tech, byte[] bytes) throws IOException {
        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_NFC_F)) throw new UnsupportedOperationException("Tag does not support NfcF");

        NfcF technology = (NfcF) tech;
        try {
            technology.connect();
            // set max response time
            technology.setTimeout(500);
            return technology.transceive(bytes);
        } finally {
            technology.close();
        }
    }
    //endregion ITagTechnologyWrapper implementation

    //region private functions
    /**
     * Reads the given blocks, (maximum of 4 blocks at a time).
     * Prerequisites: Tech must be connected first!
     * @param tech Technology to read from
     * @param blocks Addresses of blocks to read
     * @param id Id of tag
     * @return Bytes read from the tag
     * @throws IOException
     */
    private byte[] readBlocks(NfcF tech, int[] blocks, byte[] id) throws IOException {
        if(blocks == null) throw new NullPointerException("blocks");
        if(blocks.length > 4) throw new IllegalArgumentException("blocks.length > 4");

        ByteArrayOutputStream commandStream = new ByteArrayOutputStream();
        commandStream.write(CMD_READ_WO_ENCRYPTION);    // Set command
        commandStream.write(id);                        // Set tag id
        commandStream.write(0x01);                      // Number of service code
        commandStream.write(R_SERVICE_CODE);            // Service code
        commandStream.write(blocks.length);             // Number of blocks
        // Add blocklist
        for(int block : blocks) {
            if(block > 0xFF){
                commandStream.write(new byte[]{0x00, (byte) block, (byte) (block >> 8)});
            } else {
                commandStream.write(new byte[]{(byte) 0x80, (byte) block});
            }
        }
        byte[] cmd = commandStream.toByteArray();
        cmd = ArrayUtils.add(cmd, 0, (byte)(cmd.length + 1));   // add length to the start
        byte[] res = tech.transceive(cmd);                      // Send command
        return ArrayUtils.subarray(res, 13, res.length);        // Return read bytes
    }

    /**
     * Reads the given blocks, (maximum of 4 blocks at a time).
     * Prerequisites: Tech must be connected first!
     * @param tech Technology to write to
     * @param blocks Addresses of blocks to read
     * @param id Id of tag
     * @return Bytes read from the tag
     * @throws IOException
     */
    private void writeBlocks(NfcF tech, int[] blocks, byte[] data, byte[] id) throws IOException {

        if(blocks.length * BLOCK_SIZE != data.length) throw new IllegalArgumentException("blocks.length * BLOCK_SIZE != data.length");

        ByteArrayOutputStream commandStream = new ByteArrayOutputStream();
        commandStream.write(CMD_WRITE_WO_ENCRYPTION);    // Set command
        commandStream.write(id);                        // Set tag id
        commandStream.write(0x01);                      // Number of service code
        commandStream.write(RW_SERVICE_CODE);           // Service code
        commandStream.write(blocks.length);             // Number of blocks
        // Add blocklist
        for(int block : blocks) {
            if(block > 0xFF){
                commandStream.write(new byte[]{0x00, (byte) block, (byte) (block >> 8)});
            } else {
                commandStream.write(new byte[]{(byte) 0x80, (byte) block});
            }
        }
        // Add bytes
        commandStream.write(data);
        byte[] cmd = commandStream.toByteArray();
        cmd = ArrayUtils.add(cmd, 0, (byte)(cmd.length + 1));   // add length to the start
        byte[] res = tech.transceive(cmd);                      // Send command

        if(res == null) throw new IOException("Error in transmission");
        if(res.length != 12) throw new IOException("Unexpected return length");
        if(res[1] != 0x09) throw new IOException("Unexpected result");
        decipherFlags(res[10], res[11]);
    }

    /**
     * Throws exception with fitting description based on flags.
     * @param f1 flag 1
     * @param f2 flag 2
     * @throws IOException Exception thrown if error is reported in flags.
     */
    private void decipherFlags(byte f1, byte f2) throws IOException {
        if(f1 == 0x00 && f2 == 0x00) return;    // Nothing to report
        if(f1 == 0xff) return;                  // Just warning

        int block = f1 == 0xff ? -1 : f1;

        switch (f2) {
            case (byte) 0xA1 : throw new IOException("Number of services is not 01h");
            case (byte) 0xA2 : throw new IOException("Number of blocks is not 01h");
            case (byte) 0xA7 : throw new IOException("Access Mode of Block List is not 000b");
            case (byte) 0xA3 : throw new IOException("Order of Block List in Service Code List is not 0000b");
            case (byte) 0xA6 : throw new IOException("Access attribute in Service Code is not 001001b(RW)");
            case (byte) 0xA8 : throw new IOException("The Block identified with Block Number of Block List is not RW area.");
            case (byte) 0xA9 : throw new IOException("Counter write value is greater than the current counter write value.");
            case (byte) 0xB1 : throw new IOException("Error.");
            case (byte) 0xB2 : throw new IOException("Error.");
            case (byte) 0x70 : throw new IOException("Memory error (fatal error).");
            //case (byte) 0x71 : throw new IOException("The number of memory rewrites exceeds the upper limit (this is only a warning; data writing is performed as normal");
            case (byte) 0x71 : MessengerService.Default.send(new InformationMessage("The number of memory rewrites exceeds the upper limit (this is only a warning; data writing is performed as normal"));
            default: throw new IOException("Unknown response to IO operation from tag.");
        }


    }

    //endregion private functions
}
