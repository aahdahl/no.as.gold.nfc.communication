package no.as.gold.nfc.communication.technologies;

import android.nfc.tech.MifareUltralight;
import android.nfc.tech.NfcA;
import android.nfc.tech.TagTechnology;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import no.as.gold.nfc.communication.Utils.Utils;

/**
 * Created by Aage Dahl on 13.02.14.
 */
public class Tag2NfcAWrapper implements ITagTechnologyWrapper {

    private static int PAGE_SIZE = 4;

    private static byte READ = 0x30;
    private static byte WRITE = (byte)0xA2;
    private static Collection<Byte> ROM_BLOCKS = Arrays.asList(new Byte[] {0x00, 0x01, 0x02, 0x03});

    // Available for RW: block 4 -> 15
    private final static int MIN_CONTIGUOUS_RW_ADDR = 4*4;
    private final static int MAX_CONTIGUOUS_RW_ADDR = 16*4 - 1;

    private NfcA mTech;

    //region ITagTechnologyWrapper implementation
    @Override
    public void connect(TagTechnology tech) throws IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof NfcA)) throw new UnsupportedOperationException("tech");

        mTech = (NfcA) tech;
        mTech.connect();
    }

    @Override
    public void close() throws IOException {
        mTech.close();
    }

    @Override
    public byte[] read(int length, int address) throws UnsupportedOperationException, NullPointerException, IOException {
        // number of pages to read...
        int firstPage = address / MifareUltralight.PAGE_SIZE;
        int lastPage = (int)Math.ceil((double)(address + length) / MifareUltralight.PAGE_SIZE);

        ByteBuffer bytes = ByteBuffer.allocate(4 * (lastPage - firstPage) * MifareUltralight.PAGE_SIZE);
        // readPages reads 4 pages at a time
        for(int i = firstPage; i < lastPage; i+=4) {
            bytes.put(mTech.transceive(new byte[]{READ, (byte) (i * 4)}));
        }
        // Get the desired sub-set of the read bytes
        int start = address % PAGE_SIZE;
        int end = start + length;
        return Arrays.copyOfRange(bytes.array(), start, end);
    }

    @Override
    public byte[] read(TagTechnology tech, int length, int byteOffset) throws UnsupportedOperationException, NullPointerException, IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof NfcA)) throw new UnsupportedOperationException("tech");

        // get
        NfcA technology = (NfcA) tech;

        // number of pages to read...
        int firstPage = byteOffset / MifareUltralight.PAGE_SIZE;
        int lastPage = (int)Math.ceil((double)(byteOffset + length) / MifareUltralight.PAGE_SIZE);

        ByteBuffer bytes = ByteBuffer.allocate(4 * (lastPage - firstPage) * MifareUltralight.PAGE_SIZE);
        // readPages reads 4 pages at a time
        try {
            technology.connect();
            for(int i = firstPage; i < lastPage; i+=4) {
                bytes.put(technology.transceive(new byte[]{READ, (byte) (i * 4)}));
            }
        } finally {
            technology.close();
        }
        // Get the desired sub-set of the read bytes
        int start = byteOffset % PAGE_SIZE;
        int end = start + length;
        return Arrays.copyOfRange(bytes.array(), start, end);
    }

    @Override
    public byte[] readAll(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        return read(tech, (getMaxContiguousRWAddress() - getMinContiguousRWAddress()), getMinContiguousRWAddress());
    }

    @Override
    public byte[] readAll() throws UnsupportedOperationException, NullPointerException, IOException {
        return read((getMaxContiguousRWAddress() - getMinContiguousRWAddress()), getMinContiguousRWAddress());
    }

    @Override
    public void write(TagTechnology tech, byte[] bytes, int byteOffset) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof NfcA)) throw new UnsupportedOperationException("tech");

        // get technology
        NfcA technology = (NfcA) tech;

        // number of pages to read...
        int firstPage = byteOffset / MifareUltralight.PAGE_SIZE;
        int lastPage = (int)Math.ceil((double)(byteOffset + bytes.length) / PAGE_SIZE);

        ByteBuffer byteBuffer = ByteBuffer.allocate((lastPage - firstPage) * PAGE_SIZE);
        // pad with 0's
        for(int i = 0; i < bytes.length; i++) {
            byteBuffer.put(i + byteOffset % PAGE_SIZE, bytes[i]);
        }

        // writes one page at a time... (note: if one byte is sent, the remainder of the page will be padded...)
        try {
            technology.connect();
            byte[] data = byteBuffer.array();
            for(int i = firstPage; i < lastPage; i++) {
                int offset = (i-firstPage) * PAGE_SIZE;
                if(ROM_BLOCKS.contains(new Byte((byte)i))) throw new IOException("Write operation to protected area!");
                technology.transceive(new byte[] {WRITE, (byte) i, data[offset], data[offset+1], data[offset+2], data[offset+3]});
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            technology.close();
        }
    }

    @Override
    public void write(byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        // number of pages to read...
        int firstPage = address / MifareUltralight.PAGE_SIZE;
        int lastPage = (int)Math.ceil((double)(address + bytes.length) / PAGE_SIZE);

        ByteBuffer byteBuffer = ByteBuffer.allocate((lastPage - firstPage) * PAGE_SIZE);
        // pad with 0's
        for(int i = 0; i < bytes.length; i++) {
            byteBuffer.put(i + address % PAGE_SIZE, bytes[i]);
        }

        // writes one page at a time... (note: if one byte is sent, the remainder of the page will be padded...)
        byte[] data = byteBuffer.array();
        for(int i = firstPage; i < lastPage; i++) {
            int offset = (i-firstPage) * PAGE_SIZE;
            if(ROM_BLOCKS.contains(new Byte((byte)i))) throw new IOException("Write operation to protected area!");
            mTech.transceive(new byte[] {WRITE, (byte) i, data[offset], data[offset+1], data[offset+2], data[offset+3]});
        }
    }

    @Override
    public double timeRTT(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof MifareUltralight)) throw new UnsupportedOperationException("tech");

        String uuid = UUID.randomUUID().toString();
        NfcA technology = (NfcA) tech;
        try {
            technology.connect();
            Utils.SimpleTimer.startTimer(uuid);
            technology.transceive(new byte[]{READ, (byte) (getMinContiguousRWAddress() * 4)});
            return Utils.SimpleTimer.getElapsedSeconds(uuid);
        } finally {
            Utils.SimpleTimer.removeTimer(uuid);
            technology.close();

        }
    }

    @Override
    public int getMinContiguousRWAddress() {
        return MIN_CONTIGUOUS_RW_ADDR;
    }

    @Override
    public int getMaxContiguousRWAddress() {
        return MAX_CONTIGUOUS_RW_ADDR;
    }

    @Override
    public byte[] transmitCommand(TagTechnology tech, byte[] bytes) throws IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof MifareUltralight)) throw new UnsupportedOperationException("tech");

        NfcA technology = (NfcA) tech;
        try {
            technology.connect();
            // set max response time
            technology.setTimeout(500);
            return technology.transceive(bytes);
        } finally {
            technology.close();
        }
    }
    //endregion ITagTechnologyWrapper implementation
}
