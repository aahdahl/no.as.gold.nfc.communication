package no.as.gold.nfc.communication.technologies;

import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcF;
import android.nfc.tech.TagTechnology;

import java.util.Arrays;
import java.util.Collection;

import no.as.gold.nfc.communication.Utils.Identifiers;

/**
 * Created by Aage Dahl on 03.03.14.
 */
public class WrapperFactory {
    /**
     * Creates a wrapper from the given tag.
     * @param tag Tag to create wrapper from
     * @return Wrapper for communication with tag
     */
    public static ITagTechnologyWrapper getWrapper(Tag tag) throws UnsupportedOperationException{
        Collection<String> supportedTech = Arrays.asList(tag.getTechList());
        if(supportedTech.contains(Identifiers.TECH_NFC_F)) {
            return new NfcFFeliCaLiteSWrapper();
        } else if(supportedTech.contains(Identifiers.TECH_ISO_DEP)) {
            return new IsoDepMifareDesfireWrapper();
        } else if(supportedTech.contains(Identifiers.TECH_MIFARE_CLASSIC)) {
            return new MifareClassicWrapper();
        } else if(supportedTech.contains(Identifiers.TECH_MIFARE_ULTRALIGHT)) {
            return new Tag2MifareUltralightWrapper();
        } else if(supportedTech.contains(Identifiers.TECH_NFC_A)) {
            return new NfcAWrapper(tag);
        } else {
            throw new UnsupportedOperationException("Technology not supported");
        }
    }

    public static TagTechnology getTechnology(Tag tag) {
        Collection<String> supportedTech = Arrays.asList(tag.getTechList());
        if(supportedTech.contains(Identifiers.TECH_NFC_F)) {
            return NfcF.get(tag);
        } else if(supportedTech.contains(Identifiers.TECH_ISO_DEP)) {
            return IsoDep.get(tag);
        } else if(supportedTech.contains(Identifiers.TECH_MIFARE_CLASSIC)) {
            return MifareClassic.get(tag);
        } else if(supportedTech.contains(Identifiers.TECH_MIFARE_ULTRALIGHT)) {
            return MifareUltralight.get(tag);
        } else if(supportedTech.contains(Identifiers.TECH_NFC_A)) {
            return NfcA.get(tag);
        } else {
            throw new UnsupportedOperationException("Technology not supported");
        }

    }
}
