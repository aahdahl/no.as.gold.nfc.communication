package no.as.gold.nfc.communication.technologies;

import android.nfc.tech.NfcA;
import android.nfc.tech.TagTechnology;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import no.as.gold.nfc.communication.Utils.Utils;

/**
 * Created by Aage Dahl on 13.02.14.
 */
public class Tag1NfcAWrapper implements ITagTechnologyWrapper {


    private static byte RALL = 0x00;    // Read all bytes
    private static byte READ = 0x01;    // Read single byte
    private static byte WRITE_E = 0x53; // Write and erase 1 byte
    private static byte WRITE_NE = 0x1A; // Write and NOT erase 1 byte
    private NfcA mTag;
    private byte[] tagId;
    private static Collection<Byte> ROM_ADDR = Arrays.asList(new Byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                                                                         104, 105, 106, 107, 108, 109, 110, 111, 112, 113,
                                                                         114, 115, 116, 117, 118, 119, 120, 121, 122});

    // RW is possible from byte 8 to byte 103 on both static and dynamic memory on tag1 without interfering with RW access bytes.
    private final static int MIN_CONTIGUOUS_RW_ADDR = 8;
    private final static int MAX_CONTIGUOUS_RW_ADDR = 103;

    private NfcA mTech;
    private byte[] mId;

    @Override
    public void connect(TagTechnology tech) throws IOException {
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof NfcA)) throw new UnsupportedOperationException("tech");

        mTech = (NfcA) tech;
        mTech.connect();

        // set max response time
        mTech.setTimeout(500);
        mId = getTagId(mTech);
    }

    @Override
    public void close() throws IOException {
        mTech.close();
    }

    @Override
    public byte[] read(int length, int address) throws UnsupportedOperationException, NullPointerException, IOException {

        ByteBuffer byteBuffer = ByteBuffer.allocate(length);

        //read all bytes
        for(int i = 0; i < length; i++) {
            byte[] cmd = new byte[] {READ, (byte) (address + i), 0x00, mId[0], mId[1], mId[2], mId[3]};
            byte[] res = mTech.transceive(cmd);
            byteBuffer.put(res[0]);
        }
        return byteBuffer.array();
    }

    //region ITagTechnologyWrapper implementation
    @Override
    public byte[] read(TagTechnology tech, int length, int address) throws UnsupportedOperationException, NullPointerException, IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof NfcA)) throw new UnsupportedOperationException("tech");

        NfcA tag = (NfcA) tech;

        try {
            byte[] id = getTagId(tag);

            ByteBuffer byteBuffer = ByteBuffer.allocate(length);
            tag.connect();
            // set max response time
            tag.setTimeout(500);

            //read all bytes
            for(int i = 0; i < length; i++) {
                byte[] cmd = new byte[] {READ, (byte) (address + i), 0x00, id[0], id[1], id[2], id[3]};
                byte[] res = tag.transceive(cmd);
                byteBuffer.put(res[0]);
            }
            return byteBuffer.array();
        } finally {tag.close();}
    }

    @Override
    public byte[] readAll(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof NfcA)) throw new UnsupportedOperationException("tech");

        NfcA tag = (NfcA) tech;

        // Send command and return response
        try {
            byte[] id = getTagId(tag);
            byte[] cmd = new byte[] {RALL, 0x00, 0x00, id[0], id[1], id[2], id[3]};
            tag.connect();
            return tag.transceive(cmd);
        } finally { tag.close();}
    }

    @Override
    public byte[] readAll() throws UnsupportedOperationException, NullPointerException, IOException {
        byte[] id = getTagId(mTech);
        byte[] cmd = new byte[] {RALL, 0x00, 0x00, id[0], id[1], id[2], id[3]};
        return mTech.transceive(cmd);
    }

    @Override
    public void write(TagTechnology tech, byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof NfcA)) throw new UnsupportedOperationException("tech");

        NfcA technology = (NfcA) tech;
        try {
            technology.connect();
            byte[] id = tech.getTag().getId();
            for(int i = 0; i < bytes.length; i++) {
                byte addr = (byte)(i + address);
                if(ROM_ADDR.contains(addr)) throw new IOException("Memory location is protected!");
                byte[] res = technology.transceive(new byte[] {WRITE_E, addr, bytes[i], id[0], id[1], id[2], id[3]});
                if(res[0] != bytes[i]) throw new IOException("Error writing to tag");
            }
        } finally {
            technology.close();
        }

    }

    @Override
    public void write(byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        for(int i = 0; i < bytes.length; i++) {
            byte addr = (byte)(i + address);
            if(ROM_ADDR.contains(addr)) throw new IOException("Memory location is protected!");
            byte[] res = mTech.transceive(new byte[] {WRITE_E, addr, bytes[i], mId[0], mId[1], mId[2], mId[3]});
            if(res[0] != bytes[i]) throw new IOException("Error writing to tag");
        }
    }

    @Override
    public double timeRTT(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof NfcA)) throw new UnsupportedOperationException("tech");

        byte id[] = getTagId(tech);
        String uuid = UUID.randomUUID().toString();
        NfcA technology = (NfcA) tech;
        try {
            technology.connect();
            Utils.SimpleTimer.startTimer(uuid);
            technology.transceive(new byte[] {READ, (byte) (getMinContiguousRWAddress()), 0x00, id[0], id[1], id[2], id[3]});
            return Utils.SimpleTimer.getElapsedSeconds(uuid);
        } finally {
            Utils.SimpleTimer.removeTimer(uuid);
            technology.close();
        }
    }

    @Override
    public int getMinContiguousRWAddress() {
        return MIN_CONTIGUOUS_RW_ADDR;
    }

    @Override
    public int getMaxContiguousRWAddress() {
        return MAX_CONTIGUOUS_RW_ADDR;
    }

    @Override
    public byte[] transmitCommand(TagTechnology tech, byte[] bytes) throws IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof NfcA)) throw new UnsupportedOperationException("tech");

        NfcA technology = (NfcA) tech;
        try {
            technology.connect();
            // set max response time
            technology.setTimeout(500);
            return technology.transceive(bytes);
        } finally {
            technology.close();
        }
    }
    //endregion ITagTechnologyWrapper implementation

    //region private functions
    private byte[] getTagId(TagTechnology tech) {
        return tech.getTag().getId();
    }
    //endregion private functions
}
