package no.as.gold.nfc.communication.technologies;

import android.nfc.tech.MifareUltralight;
import android.nfc.tech.TagTechnology;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import no.as.gold.nfc.communication.Utils.Utils;

/**
 * This is a wrapper class that enables {@link android.nfc.tech.MifareUltralight} to implement {@link no.as.gold.nfc.communication.technologies.ITagTechnologyWrapper}
 * Created by Aage Dahl on 14.02.14.
 */
public class Tag2MifareUltralightWrapper implements ITagTechnologyWrapper {
    private static Collection<Byte> ROM_BLOCKS = Arrays.asList(new Byte[] {0x00, 0x01, 0x02, 0x03});

    // Available for RW: block 4 -> 15
    private final static int MIN_CONTIGUOUS_RW_ADDR = 4*4;
    //private final static int MAX_CONTIGUOUS_RW_ADDR = 16*4 - 1;
    private final static int MAX_CONTIGUOUS_RW_ADDR = 40*4 - 1;

    private MifareUltralight mTech;
    @Override
    public void connect(TagTechnology tech) throws IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof MifareUltralight)) throw new UnsupportedOperationException("tech");

        mTech = (MifareUltralight) tech;
        mTech.connect();
    }

    @Override
    public void close() throws IOException {
        mTech.close();
    }

    @Override
    public byte[] read(int length, int address) throws UnsupportedOperationException, NullPointerException, IOException {
        // number of pages to read...
        int firstPage = address / MifareUltralight.PAGE_SIZE;
        int lastPage = (int)Math.ceil((double)(address + length) / MifareUltralight.PAGE_SIZE);

        ByteBuffer bytes = ByteBuffer.allocate(4 * (lastPage - firstPage) * MifareUltralight.PAGE_SIZE);
        // readPages reads 4 pages at a time
        for(int i = firstPage; i < lastPage; i+=4) {
            bytes.put(mTech.readPages(i));
        }
        // Get the desired sub-set of the read bytes
        int start = address % MifareUltralight.PAGE_SIZE;
        int end = start + length;
        return Arrays.copyOfRange(bytes.array(), start, end);
    }

    @Override
    public byte[] read(TagTechnology tech, int length, int byteOffset) throws UnsupportedOperationException, NullPointerException, IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof MifareUltralight)) throw new UnsupportedOperationException("tech");

        // get
        MifareUltralight technology = (MifareUltralight) tech;

        // number of pages to read...
        int firstPage = byteOffset / MifareUltralight.PAGE_SIZE;
        int lastPage = (int)Math.ceil((double)(byteOffset + length) / MifareUltralight.PAGE_SIZE);

        ByteBuffer bytes = ByteBuffer.allocate(4 * (lastPage - firstPage) * MifareUltralight.PAGE_SIZE);
        // readPages reads 4 pages at a time
        try {
            technology.connect();
            for(int i = firstPage; i < lastPage; i+=4) {
                bytes.put(technology.readPages(i));
            }
        } finally {
            technology.close();
        }
        // Get the desired sub-set of the read bytes
        int start = byteOffset % MifareUltralight.PAGE_SIZE;
        int end = start + length;
        return Arrays.copyOfRange(bytes.array(), start, end);
    }

    @Override
    public byte[] readAll(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        return read(tech, getMaxContiguousRWAddress() - getMinContiguousRWAddress(), getMinContiguousRWAddress());
    }

    @Override
    public byte[] readAll() throws UnsupportedOperationException, NullPointerException, IOException {
        return read(getMaxContiguousRWAddress() - getMinContiguousRWAddress(), getMinContiguousRWAddress());
    }

    @Override
    public void write(TagTechnology tech, byte[] bytes, int byteOffset) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof MifareUltralight)) throw new UnsupportedOperationException("tech");

        // get technology
        MifareUltralight technology = (MifareUltralight) tech;

        // number of pages to read...
        int firstPage = byteOffset / MifareUltralight.PAGE_SIZE;
        int lastPage = (int)Math.ceil((double)(byteOffset + bytes.length) / MifareUltralight.PAGE_SIZE);

        ByteBuffer byteBuffer = ByteBuffer.allocate((lastPage - firstPage) * MifareUltralight.PAGE_SIZE);
        // pad with 0's
        for(int i = 0; i < bytes.length; i++) {
            byteBuffer.put(i + byteOffset % MifareUltralight.PAGE_SIZE, bytes[i]);
        }

        // writes one page at a time... (note: if one byte is sent, the remainder of the page will be padded...)
        try {
            technology.connect();
            for(int i = firstPage; i < lastPage; i++) {
                if(ROM_BLOCKS.contains(new Byte((byte)i))) throw new IOException("Write operation to protected area!");
                technology.writePage(i, Arrays.copyOfRange(byteBuffer.array(), (i-firstPage)*MifareUltralight.PAGE_SIZE,(i+1-firstPage)*MifareUltralight.PAGE_SIZE));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            technology.close();
        }
    }

    @Override
    public void write(byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        // number of pages to read...
        int firstPage = address / MifareUltralight.PAGE_SIZE;
        int lastPage = (int)Math.ceil((double)(address + bytes.length) / MifareUltralight.PAGE_SIZE);

        ByteBuffer byteBuffer = ByteBuffer.allocate((lastPage - firstPage) * MifareUltralight.PAGE_SIZE);
        // pad with 0's
        for(int i = 0; i < bytes.length; i++) {
            byteBuffer.put(i + address % MifareUltralight.PAGE_SIZE, bytes[i]);
        }

        // writes one page at a time... (note: if one byte is sent, the remainder of the page will be padded...)
        for(int i = firstPage; i < lastPage; i++) {
            if(ROM_BLOCKS.contains(new Byte((byte)i))) throw new IOException("Write operation to protected area!");
            mTech.writePage(i, Arrays.copyOfRange(byteBuffer.array(), (i - firstPage) * MifareUltralight.PAGE_SIZE, (i + 1 - firstPage) * MifareUltralight.PAGE_SIZE));
        }
    }

    @Override
    public double timeRTT(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof MifareUltralight)) throw new UnsupportedOperationException("tech");

        String uuid = UUID.randomUUID().toString();
        MifareUltralight technology = (MifareUltralight) tech;
        try {
            technology.connect();
            Utils.SimpleTimer.startTimer(uuid);
            technology.readPages(getMinContiguousRWAddress());
            return Utils.SimpleTimer.getElapsedSeconds(uuid);
        } finally {
            Utils.SimpleTimer.removeTimer(uuid);
            technology.close();

        }
    }

    @Override
    public int getMinContiguousRWAddress() {
        return MIN_CONTIGUOUS_RW_ADDR;
    }

    @Override
    public int getMaxContiguousRWAddress() {
        return MAX_CONTIGUOUS_RW_ADDR;
    }

    @Override
    public byte[] transmitCommand(TagTechnology tech, byte[] bytes) throws IOException {
        // Check input
        if(tech == null) throw new NullPointerException("tech");
        if(!(tech instanceof MifareUltralight)) throw new UnsupportedOperationException("tech");

        MifareUltralight technology = (MifareUltralight) tech;
        try {
            technology.connect();
            // set max response time
            technology.setTimeout(500);
            return technology.transceive(bytes);
        } finally {
            technology.close();
        }
    }
}
