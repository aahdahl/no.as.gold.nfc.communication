package no.as.gold.nfc.communication;


import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.UUID;
import java.util.zip.CRC32;

import no.as.gold.nfc.communication.Utils.Utils;
import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.simplemessenger.MessengerService;

/**
 * Asynchronously reads bytes from the given tag
 * On execute this function reads all records in the given tag and returns them as a 2D array of bytes (one array per record)
 * Upon completing the reading task, a message containing the result is sent to {@link MessengerService}
 * @author Aage Dahl
 *
 */
public class NdefReaderTask extends AsyncTask<Object, Void, ComHandlerMessage> {

    //region AsyncTask implementation
    @Override
    protected ComHandlerMessage doInBackground(Object... params) {
        Tag tag;
        Ndef ndef;
        int paramIndex = 0;
        UUID initiatorUUID = null;

        // Assert enough parameters where supplied
        if(params == null || params.length < 1) {
            return new ComHandlerMessage(initiatorUUID, "Parameters do not contain a Tag.", ComHandlerMessage.StatusOptions.READ_PARAMETER_ERROR);
        }

        // Assert that first parameter is a tag
        if(!(params[paramIndex] instanceof Tag)) {
            return new ComHandlerMessage(initiatorUUID, "First argument must be an instance of type " + Tag.class.toString(), ComHandlerMessage.StatusOptions.READ_PARAMETER_ERROR);
        }
        tag = (Tag) params[paramIndex++];

        // Get ndef from tag
        if((ndef = Ndef.get(tag)) == null) {
            return new ComHandlerMessage(initiatorUUID, "Tag does not contain a Ndef message.", ComHandlerMessage.StatusOptions.READ_PARAMETER_ERROR);
        }

        // Mark recipient with UUID
        if(params.length > 1 && params[paramIndex] instanceof UUID) {
            initiatorUUID = (UUID) params[paramIndex++];
        }

        NdefMessage message = null;
        double transferTime;
        try
        {
            Utils.SimpleTimer.startTimer(initiatorUUID.toString());
            ndef.connect();
            message = ndef.getNdefMessage();
            ndef.close();
            transferTime = Utils.SimpleTimer.getElapsedSeconds(initiatorUUID.toString());
        }
        catch (IOException e1)
        {
            // Inform whoever is interested that the read operation failed due to connection problems
            return new ComHandlerMessage(initiatorUUID, ComHandlerMessage.StatusOptions.CONNECTION_ERROR);
        }
        catch (FormatException e1)
        {
            return new ComHandlerMessage(initiatorUUID, ComHandlerMessage.StatusOptions.READ_FAILED);
        } catch (Exception e1) {
            return new ComHandlerMessage(initiatorUUID, "NdefReaderTask.DoInBackground() error: " + e1.getMessage(), ComHandlerMessage.StatusOptions.READ_FAILED);

        } finally {
            if(ndef != null) {
                try {
                    ndef.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Utils.SimpleTimer.removeTimer(initiatorUUID.toString());
        }

        if(message == null)
        {
            return new ComHandlerMessage(initiatorUUID, ComHandlerMessage.StatusOptions.READ_SUCCESS);
        }

        // Check CRC
        if(!Utils.NdefUtils.checkNdefCrc(message)) {
            // CRC error
            return new ComHandlerMessage(initiatorUUID, "CRC error", ComHandlerMessage.StatusOptions.READ_FAILED);
        }

        // Add stats for transfer time/speed
        ComHandlerMessage comMessage = new ComHandlerMessage(initiatorUUID, message);
        comMessage.TransferSizeInBytes = message.getByteArrayLength();
        comMessage.TransferTimeInSeconds = transferTime;

        return comMessage;
    }

    @Override
    /**
     * Simply transmits the message with the UI thread
     */
    protected void onPostExecute(ComHandlerMessage message) {
        MessengerService.Default.send(message);
    }
    //endregion
}
