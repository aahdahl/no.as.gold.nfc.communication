package no.as.gold.nfc.communication.Utils;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.CRC32;

import no.as.gold.nfc.communication.messages.ComHandlerMessage;

/**
 * Created by aage on 25.11.13.
 */
public class Utils {
    /**
     * Simple implementation of a timer utility class that enables starting and stopping of timers
     */
    public static class SimpleTimer {
        private static HashMap<String, Long> mStartTimers = new HashMap<String, Long>();

        /**
         * Initiates a new timer
         * @param timerId Id of the new timer
         */
        public static void startTimer(String timerId) {
            mStartTimers.put(timerId, System.nanoTime());
        }

        /**
         * Gets the time elapsed for the given timer.
         * @param timerId Id of the timer to get elapsed time from
         * @return Elapsed time in seconds
         * @throws Exception If timerId does not exist
         */
        public static double getElapsedSeconds(String timerId)
        {
            if(!mStartTimers.containsKey(timerId)) return 0;

            double startTime = mStartTimers.get(timerId);
            double elapsedTime = (System.nanoTime() - startTime);
            return elapsedTime/1000000000;
        }

        /**
         * Removes a timer. Should be called when the timer is no longer in use
         * @param timerId Id of the timer to stop
         */
        public static void removeTimer(String timerId) {
            mStartTimers.remove(timerId);
        }
    }
    public static class NdefUtils {
        /**
         * Creates a NdefMessage with the given text
         * @return NdefMessage containing the text
         */
        public static NdefMessage getTextAsNdefMessage(String text) {
            // Add text to buffer
            String formattedText = formatText(text);
            byte[] bytes = formattedText.getBytes(Charset.defaultCharset());
            ByteBuffer buffer = ByteBuffer.allocate(bytes.length + 8);
            buffer.put(bytes);

            // Add CRC to buffer
            CRC32 crc = new CRC32();
            crc.update(bytes);
            buffer.putLong(crc.getValue());

            // Generate and transmit package
            return new NdefMessage(
                    new NdefRecord[] {
                            new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
                                    NfcAdapter.EXTRA_TAG.getBytes(), new byte[0],
                                    buffer.array())});
        }

        /**
         * Reads the bytes from the NdefMessage and converts them (not including crc) back to
         * @param msg
         * @return
         */
        public static String getNdefMessageAsText(NdefMessage msg) {
            byte[] bytes = getAllNdefMessageBytes(msg);
            return getNdefMessage(bytes);
        }

        /**
         * Extracts the CRC from a message
         * @param msg Message to extract CRC from
         * @return CRC of message
         */
        public static CRC32 getCRC(NdefMessage msg) {
            if(msg == null)
                return null;

            byte[] bytes = getAllNdefMessageBytes(msg);
            CRC32 calculatedCRC = new CRC32();
            calculatedCRC.update(bytes, 0, bytes.length - 8);
            return calculatedCRC;
        }

        /**
         * Asserts that the CRC of this message (last 8 bytes) corresponds to the calculated CRC of this message.
         * @param msg Message to be analyzed
         * @return True if CRC is correct
         */
        public static boolean checkNdefCrc(NdefMessage msg) {

            // Check crc
            byte[] bytes = getAllNdefMessageBytes(msg);
            long readCRC = ByteBuffer.allocate(bytes.length).put(bytes).getLong(bytes.length - 8);

            return readCRC == getCRC(msg).getValue();
        }

        //region private functions
        /**
         * Extracts the Ndef formatted message form the byte array
         * @param bytes Bytes to decipher
         * @return Text contained in the byte array
         */
        private static String getNdefMessage(byte[] bytes) {
        /*
         * See NFC forum specification for "Text Record Type Definition" at 3.2.1
         *
         * http://www.nfc-forum.org/specs/
         *
         * bit_7 defines encoding
         * bit_6 reserved for future use, must be 0
         * bit_5..0 length of IANA language code
         */

            if(bytes == null || bytes.length < 1 ) return "Empty load";

            // Get text encoding
            String encoding = ((bytes[0] & 128) == 0) ? "UTF-8":"UTF-16";

            // Language code
            int languageCode = bytes[0] & 0063;
            languageCode = 3;

            // Ndef format WITH CRC code in last 8 bytes! (CRC code is not standard!)
            return new String(bytes, languageCode, bytes.length - languageCode - 8, Charset.defaultCharset());
        }

        /**
         * This functions adds prefixes to the text, i.e. formats it according to required format
         * @param text - text to be written to the
         * @return text formatted for writing on the tag
         */
        private static String formatText(String text) {

            // Encoding - default for android is UTF-8
            byte encoding = (byte) (Charset.defaultCharset().toString().equals("UTF-8") ? 0:1);

            // Language code
            String language = "nb";
            byte languageCode = (byte)(language.length() & 0063);

            byte descriptiveByte = (byte)(encoding | languageCode);
            // Text header
            String textHeader = (char)descriptiveByte + language;

            return textHeader + text;
        }

        private static byte[] getAllNdefMessageBytes(NdefMessage msg) {
            if(msg == null) throw new InvalidParameterException("msg");

            NdefRecord[] records = msg.getRecords();

            // Extract all byte arrays
            byte[][] result = new byte[records.length][];
            ArrayList<Byte> byteList = new ArrayList<Byte>();
            // Read all available bytes
            for(int i = 0; i < records.length; i++) {
                result[i] = records[i].getPayload();
                // Add all bytes to one large array
                for(byte b : result[i])
                    byteList.add(b);
            }

            // Insert the 2d byte array into a 1d byte array
            ByteBuffer bytes = ByteBuffer.allocate(byteList.size());
            for(Byte b : byteList)
                bytes.put(b);

            return bytes.array();
        }
        //endregion
    }
}
