package no.as.gold.nfc.communication.technologies;

import android.nfc.tech.IsoDep;
import android.nfc.tech.TagTechnology;

import org.apache.commons.lang3.ArrayUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.UUID;

import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.nfc.communication.Utils.Utils;

/**
 * Created by Aage Dahl on 26.02.14.
 * This is a ad-hoc solution, its functionality is tailored to the tag in my possession.
 * Note:
 * - Tag must have app 1 installed, if not this will be created (not tested!)
 * - Tag must have file 2, if not this will be created (not tested!)
 * - Tag must support Mifare Desfire commands
 * Potential improvements:
 * Make a more general implementation that work with ISO commands instead
 */
public class IsoDepMifareDesfireWrapper implements ITagTechnologyWrapper {

    private static byte CLA = 0x00;
    private static byte INS_SELECT = (byte)0xA4;
    private static byte INS_READ = (byte)0xB0;
    private static byte INS_WRITE = (byte)0xD0;

    //region desfire commands
    private final static byte[] APP_ID = new byte[] {0x01, 0x00, 0x00};
    private static byte[] CMD_CREATE_APP = new byte [] {
                                                        (byte)0xCA, // Create app command
                                                              APP_ID[0], APP_ID[1], APP_ID[2],//0x0F, // app id
                                                        (byte)0x0F, // Key sett
                                                        (byte)0x01  // Number of keys
    };
    private static byte[] CMD_DELETE_APP = new byte[] {(byte)0xDA, APP_ID[0], APP_ID[1], APP_ID[2]};
    private static byte[] CMD_SELECT_APP = new byte[] {(byte)0x5A, APP_ID[0], APP_ID[1], APP_ID[2]};
    private static byte[] CMD_SELECT_PICC_APP = new byte[] {0x5A, 0x00, 0x00, 0x00};

    private static byte CMD_GET_FILE_IDS = (byte)0x6F;
    private static byte[] CMD_CREATE_FILE = new byte[] {(byte)  0xCD,   // Create file command
                                                                0x02,   // File no 0
                                                                0x00,   // Com settings (chap 3.2)
                                                                0x00,   // Access rights (chap 3.3)
                                                                0x0E,   // Access rights - free r/w access
                                                        (byte)  0x00,   // File size - 3 bytes define size in bytes: set here to 512 bytes
                                                                0x02,   // File size
                                                                0x00,   // File size
                                                                        };
    private static byte CMD_VERSION_INF = (byte)0x6F;

    private static byte[] READ_COMMAND = new byte[] {CLA,       // Always 0
                                                     INS_READ,  // Read instruction
                                                     0x00,      // Lc field
                                                     0x00,      // Data field
                                                     0x00};     // Le field -> read all available bytes

    private static byte[] WRITE_COMMAND = new byte[] {CLA,       // Always 0
            INS_WRITE,  // Read instruction
            0x00,      // Lc field
            0x00,      // Data field
            0x00};     // Le field -> read all available bytes

    //endregion desfire commands

    //region desfire responces
    private final static byte RES_ADDITIONAL_FRAME_EXPECTED = (byte) 0xAF;
    //endregion
    public final static int MIN_CONTIGUOUS_RW_ADDR = 0;
    public final static int MAX_CONTIGUOUS_RW_ADDR = 512;

    private IsoDep mTech;
    //region ITagTechnology implementation
    @Override
    public void connect(TagTechnology tech) throws IOException {
        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_ISO_DEP)) throw new UnsupportedOperationException("Tag does not support IsoDep");

        mTech = (IsoDep)tech;
        mTech.connect();
    }

    @Override
    public void close() throws IOException {
        mTech.close();
    }

    @Override
    public byte[] read(int length, int address) throws UnsupportedOperationException, NullPointerException, IOException {
        // select app
        selectApp(mTech);

        // Create file if it not already exists
        checkIfFileExists(mTech, (byte) 0x02);

        // Extract offset and length as byte arrays (only 3 bytes will be used)
        byte[] offset = ByteBuffer.allocate(4).putInt(address).array();
        byte[] len = ByteBuffer.allocate(4).putInt(length).array();

        // Create a stream for output
        ByteArrayOutputStream stream;
        // 0 -> read all available data!
        if(length != 0)
            stream = new ByteArrayOutputStream(length);
        else
            stream = new ByteArrayOutputStream();

        // Send read command and receive the results
        byte[] res = mTech.transceive(new byte[]{(byte)0xBD, 0x02, offset[3], offset[2], offset[1], len[3], len[2], len[1]});
        stream.write(res, 1, res.length - 1);

        // If length > 59, multiple reads are required!
        while(res[0] == RES_ADDITIONAL_FRAME_EXPECTED) {
            res = mTech.transceive(new byte[]{RES_ADDITIONAL_FRAME_EXPECTED});
            stream.write(res, 1, res.length - 1);
        }
        // assert result was ok
        decipherResponce(res);

        //return data.array();
        return stream.toByteArray();
    }

    @Override
    public byte[] read(TagTechnology tech, int length, int address) throws UnsupportedOperationException, NullPointerException, IOException {
        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_ISO_DEP)) throw new UnsupportedOperationException("Tag does not support IsoDep");

        IsoDep technology = (IsoDep) tech;

        // Mifare desfire command to get all application id's available
        try {
            technology.connect();
            // select app
            selectApp(technology);

            // Create file if it not already exists
            checkIfFileExists(technology, (byte) 0x02);

            // Extract offset and length as byte arrays (only 3 bytes will be used)
            byte[] offset = ByteBuffer.allocate(4).putInt(address).array();
            byte[] len = ByteBuffer.allocate(4).putInt(length).array();

            // Create a stream for output
            ByteArrayOutputStream stream;
            // 0 -> read all available data!
            if(length != 0)
                stream = new ByteArrayOutputStream(length);
            else
                stream = new ByteArrayOutputStream();

            // Send read command and receive the results
            byte[] res = technology.transceive(new byte[]{(byte)0xBD, 0x02, offset[3], offset[2], offset[1], len[3], len[2], len[1]});
            stream.write(res, 1, res.length - 1);

            // If length > 59, multiple reads are required!
            while(res[0] == RES_ADDITIONAL_FRAME_EXPECTED) {
                res = technology.transceive(new byte[]{RES_ADDITIONAL_FRAME_EXPECTED});
                stream.write(res, 1, res.length - 1);
            }
            // assert result was ok
            decipherResponce(res);

            //return data.array();
            return stream.toByteArray();

        }finally {
            technology.close();
        }
    }

    private void checkIfFileExists(IsoDep technology, byte fileNo) throws IOException {
        byte[] ids = technology.transceive(new byte[] {CMD_GET_FILE_IDS});
        for(int i = 1; i < ids.length; i++) {
            if(ids[i] == fileNo ) return;
        }
        // file does not exist -> create it!
        decipherResponce(technology.transceive(CMD_CREATE_FILE));
    }

    @Override
    public byte[] readAll(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        return read(tech, (getMaxContiguousRWAddress() - getMinContiguousRWAddress()), getMinContiguousRWAddress());
    }

    @Override
    public byte[] readAll() throws UnsupportedOperationException, NullPointerException, IOException {
        return read(getMaxContiguousRWAddress() - getMinContiguousRWAddress(), getMinContiguousRWAddress());
    }

    @Override
    public void write(TagTechnology tech, byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_ISO_DEP)) throw new UnsupportedOperationException("Tag does not support IsoDep");

        IsoDep technology = (IsoDep) tech;

        // Mifare desfire command to get all application id's available
        try {
            technology.connect();
            // select app
            selectApp(technology);

            // Create file if it not already exists
            checkIfFileExists(technology, (byte) 0x02);

            // Extract offset and length as byte arrays (only 3 bytes will be used)
            byte[] offset = ByteBuffer.allocate(4).putInt(address).array();
            byte[] len = ByteBuffer.allocate(4).putInt(bytes.length).array();

            // Send write command and receive the results
            int start = 0;
            int end = 52 > bytes.length ? bytes.length : 52;
            byte[] cmd = ArrayUtils.addAll(new byte[]{(byte)0x3D, 0x02, offset[3], offset[2], offset[1], len[3], len[2], len[1]}, Arrays.copyOfRange(bytes,start,end));
            byte[] res = technology.transceive(cmd);

            // If length > 59, multiple reads are required!
            int i = 0;
            while(res[0] == RES_ADDITIONAL_FRAME_EXPECTED) {
                start = 52 + i*59;
                end = 52 + (i+1)*59;
                end = end < bytes.length ? end : bytes.length;
                res = technology.transceive(ArrayUtils.addAll(new byte[]{(byte)0xAF}, Arrays.copyOfRange(bytes, start, end)));
                i++;
            }
            // assert result was ok
            decipherResponce(res);

            //return data.array();

        } finally {
            technology.close();
        }

    }

    @Override
    public void write(byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        selectApp(mTech);

        // Create file if it not already exists
        checkIfFileExists(mTech, (byte) 0x02);

        // Extract offset and length as byte arrays (only 3 bytes will be used)
        byte[] offset = ByteBuffer.allocate(4).putInt(address).array();
        byte[] len = ByteBuffer.allocate(4).putInt(bytes.length).array();

        // Send write command and receive the results
        int start = 0;
        int end = 52 > bytes.length ? bytes.length : 52;
        byte[] cmd = ArrayUtils.addAll(new byte[]{(byte)0x3D, 0x02, offset[3], offset[2], offset[1], len[3], len[2], len[1]}, Arrays.copyOfRange(bytes,start,end));
        byte[] res = mTech.transceive(cmd);

        // If length > 52, multiple writes are required!
        int i = 0;
        while(res[0] == RES_ADDITIONAL_FRAME_EXPECTED) {
            start = 52 + i*59;
            end = 52 + (i+1)*59;
            end = end < bytes.length ? end : bytes.length;
            res = mTech.transceive(ArrayUtils.addAll(new byte[]{(byte)0xAF}, Arrays.copyOfRange(bytes, start, end)));
            i++;
        }
        // assert result was ok
        decipherResponce(res);
    }

    @Override
    public double timeRTT(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        String uuid = UUID.randomUUID().toString();
        IsoDep technology = (IsoDep) tech;
        try {
            technology.connect();
            Utils.SimpleTimer.startTimer(uuid);
            // Sends one byte and waits for response.
            technology.transceive(new byte[] {0x00});
            return Utils.SimpleTimer.getElapsedSeconds(uuid);
        } finally {
            Utils.SimpleTimer.removeTimer(uuid);
            technology.close();
        }
    }

    @Override
    public int getMinContiguousRWAddress() {
        return MIN_CONTIGUOUS_RW_ADDR;
    }

    @Override
    public int getMaxContiguousRWAddress() {
        return MAX_CONTIGUOUS_RW_ADDR;
    }

    @Override
    public byte[] transmitCommand(TagTechnology tech, byte[] bytes) throws IOException {

        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_ISO_DEP)) throw new UnsupportedOperationException("Tag does not support IsoDep");

        IsoDep technology = (IsoDep) tech;

        try {
            technology.connect();
            // set max response time
            technology.setTimeout(500);
            return technology.transceive(bytes);
        } finally {
            technology.close();
        }
    }
    //endregion ITagTechnology implementation

    //region private functions
    /**
     * Selects the correct app on the tag
     * @param technology IsoDep interface to tag
     */
    private void selectApp(IsoDep technology) throws IOException {
        // select the desired app
        if(technology.transceive(CMD_SELECT_APP)[0] == 0x00) return;

        // Select PICC level app
        decipherResponce(technology.transceive(CMD_SELECT_PICC_APP));
        // Create new app
        decipherResponce(technology.transceive(CMD_CREATE_APP));
        // Select the newly created app
        decipherResponce(technology.transceive(CMD_SELECT_APP));
        // Create file
        decipherResponce(technology.transceive(CMD_CREATE_FILE));
    }

    private void decipherResponce(byte[] res) throws IOException {
        if(res == null) throw new NullPointerException("res");

        switch (res[0]) {
            case (byte)0x00: return; // Successful operation
            case (byte)0x0C: return; //
            case (byte)0x0E: throw new IOException("OUT_OF_EEPROM_ERROR: Insufficient NV-Memory to complete command");
            case (byte)0x1C: throw new IOException("ILLEGAL_COMMAND_CODE: Command code not supported");
            case (byte)0x1E: throw new IOException("INTEGRITY_ERROR: CRC or MAC does not match data Padding bytes not valid");
            case (byte)0x40: throw new IOException("NO_SUCH_KEY Invalid: key number specified");
            case (byte)0x7E: throw new IOException("LENGTH_ERROR: Length of command string invalid");
            case (byte)0x9D: throw new IOException("PERMISSION_DENIED Current configuration / status does not allow the requested command");
            case (byte)0x9E: throw new IOException("PARAMETER_ERROR: Value of the parameter(s) invalid");
            case (byte)0xA0: throw new IOException("APPLICATION_NOT_FOUND: Requested AID not present on PICC");
            case (byte)0xA1: throw new IOException("APPL_INTEGRITY_ERROR Unrecoverable error within application, application will be disabled");
            case (byte)0xAE: throw new IOException("AUTHENTICATION_ERROR Current authentication status does not allow the requested command");
            case (byte)0xAF: throw new IOException("ADDITIONAL_FRAME: Additional data frame is expected to be sent");
            case (byte)0xBE: throw new IOException("BOUNDARY_ERROR: Attempt to read/write data from/to beyond the file's/record's limits. Attempt to exceed the limits of a value file.");
            case (byte)0xC1: throw new IOException("PICC_INTEGRITY_ERROR: Unrecoverable error within PICC, PICC will be disabled");
            case (byte)0xCA: throw new IOException("COMMAND_ABORTED: Previous Command was not fully completed Not all Frames were requested or provided by the PCD");
            case (byte)0xCD: throw new IOException("PICC_DISABLED_ERROR: PICC was disabled by an unrecoverable error");
            case (byte)0xCE: throw new IOException("COUNT_ERROR: Number of Applications limited to 28, no additional CreateApplication possible");
            case (byte)0xDE: throw new IOException("DUPLICATE_ERROR: Creation of file/application failed because file/application with same number already exists");
            case (byte)0xEE: throw new IOException("EEPROM_ERROR: Could not complete NV-write operation due to loss of power, internal backup/rollback mechanism activated");
            case (byte)0xF0: throw new IOException("FILE_NOT_FOUND: Specified file number does not exist");
            case (byte)0xF1: throw new IOException("FILE_INTEGRITY_ERROR: Unrecoverable error within file, file will be disabled");
        }

        //endregion private functions
    }
}
