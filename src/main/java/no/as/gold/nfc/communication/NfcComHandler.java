package no.as.gold.nfc.communication;

import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcF;
import android.nfc.tech.TagTechnology;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.nfc.communication.technologies.IsoDepMifareDesfireWrapper;
import no.as.gold.nfc.communication.technologies.MifareClassicWrapper;
import no.as.gold.nfc.communication.technologies.NfcAWrapper;
import no.as.gold.nfc.communication.technologies.NfcFFeliCaLiteSWrapper;
import no.as.gold.nfc.communication.technologies.Tag2MifareUltralightWrapper;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;

/**
 * Created by aage on 20.10.13.
 */
public class NfcComHandler {

    /**
     * This is a class that handles Ndef reading and writing to tags.
     */
    public static class NdefComHandler {
        /**
         * Writes bytes to a tag. This task is done asynchronously and will result in a ComHandlerMessage being published to MessengerService.
         * @param tag Bytes to write
         * @param initiatorUUID Universally unique identifier of object initiating this write operation
         * @param data Bytes to write
         */
        public static void Write(Tag tag, UUID initiatorUUID, byte[] data) {
            new NdefWriterTask().execute(new Object[] {tag, initiatorUUID, data});
        }

        /**
         * Writes a string to a tag. This task is done asynchronously and will result in a ComHandlerMessage being published to MessengerService.
         * @param tag Tag to write to
         * @param initiatorUUID Universally unique identifier of object initiating this write operation
         * @param data String to write
         * @throws java.util.concurrent.TimeoutException If unable to write to tag within 5 seconds
         */
        public static void Write(Tag tag, UUID initiatorUUID, String data) {
            Object[] objectArray = new Object[] {tag, initiatorUUID, data};
            new NdefWriterTask().execute(objectArray);
        }

        /**
         * Ensures that information is written to the tag, or timeout. (blocking function)
         * @param tag   Tag to write to
         * @param data  Data to write to tag, currently supports strings and byte arrays
         * @throws java.lang.IllegalArgumentException If data is not of type String or byte[]
         * @throws  java.util.concurrent.TimeoutException If timing out while waiting for confirmed write operation
         */
        public static void EnsureWrite(final Tag tag, final Object data) throws IllegalArgumentException, TimeoutException {
            if(!(data instanceof String || data instanceof byte[]))
                throw new IllegalArgumentException("Data must be of type String or byte[]");

            final Thread ownerThread = Thread.currentThread();
            final UUID uuid = UUID.randomUUID();
            final Object receiverKey = new Object();

            // Implementation of a sender thread
            Thread senderThread = new Thread() {
                @Override
                public void run() {
                    // Handles confirmation that message is received
                    MessengerService.Default.Register(receiverKey, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
                        @Override
                        public void handle(ComHandlerMessage msg) {
                            // Handle write messages
                            if (msg.Status == ComHandlerMessage.StatusOptions.WRITE_SUCCESS) {
                                // read message!
                                Read(tag, uuid);
                                return;
                            }

                            // Ensure that message is correct!
                            if (msg.Status == ComHandlerMessage.StatusOptions.READ_SUCCESS) {
                                // ensure message is what was written, if not return!
                                if (data instanceof String) {
                                    if (msg.getReadMessage().equals(data)) {
                                        // Success -> unregister listener
                                        MessengerService.Default.UnRegister(receiverKey, ComHandlerMessage.class);
                                        // Kill thread
                                        MessengerService.Default.send(new ComHandlerMessage(uuid, "Ensured write success!", ComHandlerMessage.StatusOptions.WRITE_SUCCESS));
                                        return;
                                    }
                                }
                            }
                            // Write message
                            Write(tag, uuid, (String) data);
                            MessengerService.Default.send(new ComHandlerMessage(uuid, "Ensured write first sending!", ComHandlerMessage.StatusOptions.WRITE_SUCCESS));
                        }
                    });

                    // send initial message
                    Write(tag, uuid, (String) data);
                }
            };

            //Ensure that sender thread has started
            senderThread.start();
            while(senderThread.isAlive()) Thread.yield();
            // Unable to start sender thread
            MessengerService.Default.UnRegister(receiverKey, ComHandlerMessage.class);
        }

        /**
         * Reads the data available on a tag. This task is done asynchronously and will resylt in a ComHandlerMessage being published to MessengerService.
         * @param tag Tag to read from
         */
        public static void Read(Tag tag) {
            new NdefReaderTask().execute(new Tag[] {tag});
        }

        /**
         * Reads the data available on a tag and marks it for sending to a designated receiver. This task is done asynchronously and will result in a ComHandlerMessage being published to MessengerService.
         * @param tag Tag to read from
         * @param initiatorUUID Universally unique sender ID to associate with this read operation
         */
        public static void Read(Tag tag, UUID   initiatorUUID) {
            new NdefReaderTask().execute(new Object[] {tag, initiatorUUID});
        }
    }

    /**
     * This is a class that handles reading and writing bytes to tags.
     */
    public static class ByteComHandler {
        /**
         * Writes bytes to a tag. This task is done asynchronously and will result in a ComHandlerMessage being published to MessengerService.
         * @param tag Tag to write to
         * @param selectedTech Technology to use when transferring
         * @param writeUUID Unique ID to associate this write operation with
         * @param payload Bytes to write
         * @param offset Offset into the memory of the tag where data is to be inserted.
         */
        public static void Write(Tag tag, String selectedTech, UUID writeUUID, byte[] payload, int offset) {

            if(tag == null)
                throw new NullPointerException("tag");

            // Identify selected technology
            switch(selectedTech) {
                case Identifiers.TECH_NFC_A :
                    try {
                        new NfcAWrapper(tag).write(NfcA.get(tag), payload, offset);
                        MessengerService.Default.send(new ComHandlerMessage(writeUUID, Arrays.toString(payload), ComHandlerMessage.StatusOptions.WRITE_SUCCESS));
                    } catch (Exception e) {
                        e.printStackTrace();
                        MessengerService.Default.send(new ComHandlerMessage(writeUUID, e.getMessage(), ComHandlerMessage.StatusOptions.WRITE_FAILED));
                    }
                    break;
                case Identifiers.TECH_MIFARE_ULTRALIGHT :
                    try {
                        new Tag2MifareUltralightWrapper().write(MifareUltralight.get(tag), payload, offset);
                        MessengerService.Default.send(new ComHandlerMessage(writeUUID, Arrays.toString(payload), ComHandlerMessage.StatusOptions.WRITE_SUCCESS));
                    } catch (Exception e) {
                        e.printStackTrace();
                        MessengerService.Default.send(new ComHandlerMessage(writeUUID, e.getMessage(), ComHandlerMessage.StatusOptions.WRITE_FAILED));
                    }
                    break;
                case Identifiers.TECH_ISO_DEP :
                    try {
                        new IsoDepMifareDesfireWrapper().write(IsoDep.get(tag), payload, offset);
                        MessengerService.Default.send(new ComHandlerMessage(writeUUID, Arrays.toString(payload), ComHandlerMessage.StatusOptions.WRITE_SUCCESS));
                    } catch (Exception e) {
                        e.printStackTrace();
                        MessengerService.Default.send(new ComHandlerMessage(writeUUID, e.getMessage(), ComHandlerMessage.StatusOptions.WRITE_FAILED));
                    }
                    break;
                case Identifiers.TECH_MIFARE_CLASSIC :
                    try {
                        new MifareClassicWrapper().write(MifareClassic.get(tag), payload, offset);
                        MessengerService.Default.send(new ComHandlerMessage(writeUUID, Arrays.toString(payload), ComHandlerMessage.StatusOptions.WRITE_SUCCESS));
                    } catch (Exception e) {
                        e.printStackTrace();
                        MessengerService.Default.send(new ComHandlerMessage(writeUUID, e.getMessage(), ComHandlerMessage.StatusOptions.WRITE_FAILED));
                    }
                    break;
                case Identifiers.TECH_NFC_F :
                    try {
                        new NfcFFeliCaLiteSWrapper().write(NfcF.get(tag), payload, offset);
                        MessengerService.Default.send(new ComHandlerMessage(writeUUID, Arrays.toString(payload), ComHandlerMessage.StatusOptions.WRITE_SUCCESS));
                    } catch (Exception e) {
                        e.printStackTrace();
                        MessengerService.Default.send(new ComHandlerMessage(writeUUID, e.getMessage(), ComHandlerMessage.StatusOptions.WRITE_FAILED));
                    }
                    break;
                default: throw new UnsupportedOperationException();
            }
        }

        /**
         * Writes a string to a tag. This task is done asynchronously and will result in a ComHandlerMessage being published to MessengerService.
         * @param tag Tag to write to
         * @param mWriteUUID
         * @param mTech
         * @param data String to write
         */
        public static void Write(Tag tag, UUID mWriteUUID, TagTechnology mTech, String data) {
            throw new UnsupportedOperationException();
        }

        /**
         * Writes data to a tag. This task is done asynchronously and will result in a ComHandlerMessage being broadcasted to MessengerService.
         * @param tag Tag to read from
         * @param selectedTech Technology to use when transferring
         * @param mReadUUID Unique ID to associate this read operation with
         * @param start Byte position to start reading from
         * @param length Number of bytes to read
         */
        public static void Read(Tag tag, String selectedTech, UUID mReadUUID, int start, int length) {
            // Assert that tag is not null
            if(tag == null)
                throw new NullPointerException("tag");

            // Identify selected technology
            switch(selectedTech) {
                case Identifiers.TECH_NFC_A :
                    try {
                        byte[] data = new NfcAWrapper(tag).read(NfcA.get(tag), length, start);
                        MessengerService.Default.send(new ComHandlerMessage(mReadUUID, Arrays.toString(data), ComHandlerMessage.StatusOptions.READ_SUCCESS));
                    } catch (IOException e) {
                        e.printStackTrace();
                        MessengerService.Default.send(new ComHandlerMessage(mReadUUID, e.getMessage(), ComHandlerMessage.StatusOptions.READ_FAILED));
                    }
                    break;
                case Identifiers.TECH_MIFARE_ULTRALIGHT :
                    try {
                        byte[] data = new Tag2MifareUltralightWrapper().read(MifareUltralight.get(tag), length, start);
                        MessengerService.Default.send(new ComHandlerMessage(mReadUUID, Arrays.toString(data), ComHandlerMessage.StatusOptions.READ_SUCCESS));
                    } catch (IOException e) {
                        e.printStackTrace();
                        MessengerService.Default.send(new ComHandlerMessage(mReadUUID, e.getMessage(), ComHandlerMessage.StatusOptions.READ_FAILED));
                    }
                    break;
                case Identifiers.TECH_ISO_DEP :
                    try {
                        byte[] data = new IsoDepMifareDesfireWrapper().read(IsoDep.get(tag), length, start);
                        MessengerService.Default.send(new ComHandlerMessage(mReadUUID, Arrays.toString(data), ComHandlerMessage.StatusOptions.READ_SUCCESS));
                    } catch (IOException e) {
                        e.printStackTrace();
                        MessengerService.Default.send(new ComHandlerMessage(mReadUUID, e.getMessage(), ComHandlerMessage.StatusOptions.READ_FAILED));
                    }
                    break;
                case Identifiers.TECH_MIFARE_CLASSIC :
                    try {
                        byte[] data = new MifareClassicWrapper().read(MifareClassic.get(tag), length, start);
                        MessengerService.Default.send(new ComHandlerMessage(mReadUUID, Arrays.toString(data), ComHandlerMessage.StatusOptions.READ_SUCCESS));
                    } catch (IOException e) {
                        e.printStackTrace();
                        MessengerService.Default.send(new ComHandlerMessage(mReadUUID, e.getMessage(), ComHandlerMessage.StatusOptions.READ_FAILED));
                    }
                    break;
                case Identifiers.TECH_NFC_F :
                    try {
                        byte[] data = new NfcFFeliCaLiteSWrapper().read(NfcF.get(tag), length, start);
                        MessengerService.Default.send(new ComHandlerMessage(mReadUUID, Arrays.toString(data), ComHandlerMessage.StatusOptions.READ_SUCCESS));
                    } catch (IOException e) {
                        e.printStackTrace();
                        MessengerService.Default.send(new ComHandlerMessage(mReadUUID, e.getMessage(), ComHandlerMessage.StatusOptions.READ_FAILED));
                    }
                    break;
                default: throw new UnsupportedOperationException();
            }
        }

        /**
         * Measures the RTT of communication with the given tag.
         * @param tag Tag to measure RTT with
         * @param mUUID Unique ID to distinguish responce from other responces
         */
        public static void timeRTT(Tag tag, UUID mUUID) {
            Collection<String> supportedTech = Arrays.asList(tag.getTechList());
            try {

                double rtt = -1;
                if(supportedTech.contains(Identifiers.TECH_NFC_F)) {
                    rtt = new NfcFFeliCaLiteSWrapper().timeRTT(NfcF.get(tag));
                } else if(supportedTech.contains(Identifiers.TECH_ISO_DEP)) {
                    rtt = new IsoDepMifareDesfireWrapper().timeRTT(IsoDep.get(tag));
                } else if(supportedTech.contains(Identifiers.TECH_MIFARE_CLASSIC)) {
                    rtt = new MifareClassicWrapper().timeRTT(MifareClassic.get(tag));
                } else if(supportedTech.contains(Identifiers.TECH_MIFARE_ULTRALIGHT)) {
                    rtt = new Tag2MifareUltralightWrapper().timeRTT(MifareUltralight.get(tag));
                } else if(supportedTech.contains(Identifiers.TECH_NFC_A)) {
                    rtt = new NfcAWrapper(tag).timeRTT(NfcA.get(tag));
                } else {
                    throw new UnsupportedOperationException("Technology not supported");
                }
                MessengerService.Default.send(new ComHandlerMessage(mUUID, "RTT = " + rtt + "seconds", ComHandlerMessage.StatusOptions.READ_SUCCESS));


            } catch (Exception e) {
                MessengerService.Default.send(new ComHandlerMessage(mUUID, e.getMessage(), ComHandlerMessage.StatusOptions.READ_FAILED));
            }

        }

        /**
         * This function transfers a command consisting of raw bytes directly to the tag. No SOD or EOD must be added because it will be added automatically.
         * When the transfer is completed, this function will post the results as a {@link no.as.gold.nfc.communication.messages.ComHandlerMessage} to {@link no.as.gold.simplemessenger.MessengerService}.
         * @param tag Tag to send the command to
         * @param selectedTech Technology to talk to
         * @param uuid UUID to mark the message with
         * @param command raw byte command to be transferred to the tag. (do not include SOD or EOD)
         */
        public static void transferCommand(Tag tag, String selectedTech, UUID uuid, byte[] command) throws Exception {
            // Assert that technology exists
            Collection<String> supportedTech = Arrays.asList(tag.getTechList());

            try {
                if(!supportedTech.contains(selectedTech)) throw new UnsupportedOperationException("Technology not supported by tag");
                byte[] response;

                switch (selectedTech) {
                    case Identifiers.TECH_NFC_A: response = new NfcAWrapper(tag).transmitCommand(NfcA.get(tag), command); break;
                    case Identifiers.TECH_MIFARE_ULTRALIGHT: response = new MifareClassicWrapper().transmitCommand(MifareClassic.get(tag), command); break;
                    case Identifiers.TECH_NFC_F: response = new NfcFFeliCaLiteSWrapper().transmitCommand(NfcF.get(tag), command); break;
                    case Identifiers.TECH_ISO_DEP: response = new IsoDepMifareDesfireWrapper().transmitCommand(IsoDep.get(tag), command); break;
                    case Identifiers.TECH_MIFARE_CLASSIC: response = new Tag2MifareUltralightWrapper().transmitCommand(MifareUltralight.get(tag), command); break;
                    default: throw new UnsupportedOperationException("Technology not supported");
                }

                MessengerService.Default.send(new ComHandlerMessage(uuid, response, ComHandlerMessage.StatusOptions.COMMAND_SUCCESS));

            } catch (Exception e) {
                MessengerService.Default.send(new ComHandlerMessage(uuid, e.getMessage(), ComHandlerMessage.StatusOptions.COMMAND_FAILED));
            }
        }
    }
}
