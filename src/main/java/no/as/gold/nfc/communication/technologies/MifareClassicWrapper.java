package no.as.gold.nfc.communication.technologies;

import android.nfc.tech.MifareClassic;
import android.nfc.tech.TagTechnology;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.UUID;

import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.nfc.communication.Utils.Utils;

/**
 * Created by Aage Dahl on 27.02.14.
 *
 * Communication with NXP proprietary tag using MifareClassic protocol.
 * Note: in each sector, the last block contains keys A and B, therefore these must not be written to!
 * Solution to this issue: map address space to avoid these areas.
 */
public class MifareClassicWrapper implements ITagTechnologyWrapper {

    // Available for RW: block 4 -> 15
    public final static int MIN_CONTIGUOUS_RW_ADDR = 50;
    //public final static int MAX_CONTIGUOUS_RW_ADDR = 15*3*16;
    public final static int MAX_CONTIGUOUS_RW_ADDR = 8*3*16 - 1;    // Damaged block 8 -> cannot write to sector 8!
    private MifareClassic mTech;

    //region ITagTechnologyWrapper implementation
    @Override
    public void connect(TagTechnology tech) throws IOException {
        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_MIFARE_CLASSIC)) throw new UnsupportedOperationException("Tag does not support MifareClassic");

        mTech = (MifareClassic) tech;
        mTech.connect();
    }

    @Override
    public void close() throws IOException {
        mTech.close();
    }

    @Override
    public byte[] read(int length, int address) throws UnsupportedOperationException, NullPointerException, IOException {
        // Convert address to block no and offset in block
        int startBlock, endBlock, startBlockOffset;
        startBlock = address / MifareClassic.BLOCK_SIZE;
        startBlockOffset = address - startBlock * MifareClassic.BLOCK_SIZE;
        endBlock = (address + length) / MifareClassic.BLOCK_SIZE;

        if(address < MIN_CONTIGUOUS_RW_ADDR || (address + length) > MAX_CONTIGUOUS_RW_ADDR) throw new IOException("Trying to access illegal address space");

        // Avoiding writing to last block in each sector -> need to move start and end block accordingly
        for(int i = 0; i < startBlock; i++) {
            int sector = mTech.blockToSector(i);
            int blocksInSector = mTech.getBlockCountInSector(sector);
            for(int j = 0; j < (blocksInSector - 1); j++) {
                i++;
                if(i == startBlock) break;
            }
            startBlock++;
            endBlock++;
        }

        ByteBuffer data = ByteBuffer.allocate((1 + endBlock - startBlock) * MifareClassic.BLOCK_SIZE);
        int lastSector = -1;
        for(int i = startBlock; i <= endBlock; i++) {
            int currentSector = mTech.blockToSector(i);
            if(!mTech.authenticateSectorWithKeyB(currentSector, MifareClassic.KEY_DEFAULT))
                throw new IOException("Unable to authenticate sector");
            // Avoid writing to the last block in each sector: contains keys for each sector...
            ;
            for(int j = 0; j < mTech.getBlockCountInSector(currentSector) - 1; j++) {
                // exit if finished
                if(i > endBlock) break;
                // Read from block
                data.put(mTech.readBlock(i));
                i++;
            }
            // not writing to last block -> need to increment
            endBlock++;
        }
        return Arrays.copyOfRange(data.array(), startBlockOffset, startBlockOffset + length);
    }

    @Override
    public byte[] read(TagTechnology tech, int length, int address) throws UnsupportedOperationException, NullPointerException, IOException {
        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_MIFARE_CLASSIC)) throw new UnsupportedOperationException("Tag does not support MifareClassic");

        MifareClassic technology = (MifareClassic) tech;
        // Convert address to block no and offset in block
        int startBlock, endBlock, startBlockOffset;
        startBlock = address / MifareClassic.BLOCK_SIZE;
        startBlockOffset = address - startBlock * MifareClassic.BLOCK_SIZE;
        endBlock = (address + length) / MifareClassic.BLOCK_SIZE;

        if(address < MIN_CONTIGUOUS_RW_ADDR || (address + length) > MAX_CONTIGUOUS_RW_ADDR) throw new IOException("Trying to access illegal address space");

        // Avoiding writing to last block in each sector -> need to move start and end block accordingly
        for(int i = 0; i < startBlock; i++) {
            int sector = technology.blockToSector(i);
            int blocksInSector = technology.getBlockCountInSector(sector);
            for(int j = 0; j < (blocksInSector - 1); j++) {
                i++;
                if(i == startBlock) break;
            }
            startBlock++;
            endBlock++;
        }

        ByteBuffer data = ByteBuffer.allocate((1 + endBlock - startBlock) * MifareClassic.BLOCK_SIZE);
        try {
            technology.connect();

            int lastSector = -1;
            for(int i = startBlock; i <= endBlock; i++) {
                int currentSector = technology.blockToSector(i);
                if(!technology.authenticateSectorWithKeyB(currentSector, MifareClassic.KEY_DEFAULT))
                    throw new IOException("Unable to authenticate sector");
                // Avoid writing to the last block in each sector: contains keys for each sector...
                ;
                for(int j = 0; j < technology.getBlockCountInSector(currentSector) - 1; j++) {
                    // exit if finished
                    if(i > endBlock) break;
                    // Read from block
                    data.put(technology.readBlock(i));
                    i++;
                }
                // not writing to last block -> need to increment
                endBlock++;
            }
        } finally {
            technology.close();
        }
        return Arrays.copyOfRange(data.array(), startBlockOffset, startBlockOffset + length);
    }

    @Override
    public byte[] readAll(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_MIFARE_CLASSIC)) throw new UnsupportedOperationException("Tag does not support MifareClassic");

        return read(tech, MAX_CONTIGUOUS_RW_ADDR - MIN_CONTIGUOUS_RW_ADDR, MIN_CONTIGUOUS_RW_ADDR);
    }

    @Override
    public byte[] readAll() throws UnsupportedOperationException, NullPointerException, IOException {
        return read(MAX_CONTIGUOUS_RW_ADDR - MIN_CONTIGUOUS_RW_ADDR, MIN_CONTIGUOUS_RW_ADDR);
    }

    @Override
    public void write(TagTechnology tech, byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {

        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_MIFARE_CLASSIC)) throw new UnsupportedOperationException("Tag does not support MifareClassic");

        MifareClassic technology = (MifareClassic) tech;
        // Convert address to block no and offset in block
        int startBlock, endBlock, startBlockOffset, endBlockOffset;
        startBlock = address / MifareClassic.BLOCK_SIZE;
        startBlockOffset = address - startBlock * MifareClassic.BLOCK_SIZE;
        endBlock = (address + bytes.length) / MifareClassic.BLOCK_SIZE;
        endBlockOffset = (address + bytes.length) % MifareClassic.BLOCK_SIZE;

        if(address < MIN_CONTIGUOUS_RW_ADDR || (address + bytes.length) > MAX_CONTIGUOUS_RW_ADDR) throw new IOException("Trying to access illegal address space");

        // Avoiding writing to last block in each sector -> need to move start and end block accordingly
        for(int i = 0; i < startBlock; i++) {
            int sector = technology.blockToSector(i);
            int blocksInSector = technology.getBlockCountInSector(sector);
            for(int j = 0; j < (blocksInSector - 1); j++) {
                i++;
                if(i == startBlock) break;
            }
            startBlock++;
            endBlock++;
        }

        // for each block, write 16 bytes
        byte[] block = new byte[16];
        try {
            technology.connect();

            for(int i = startBlock; i <= endBlock; i++) {
                // Authenticate sector
                int sector = technology.blockToSector(i);
                if(!technology.authenticateSectorWithKeyB(sector, MifareClassic.KEY_DEFAULT))
                    throw new IOException("Unable to authenticate sector");

                for(int k = 0; k < (technology.getBlockCountInSector(i) - 1); k++) {
                    if(i > endBlock) break;

                    // Copy bytes to block
                    int from = i == startBlock ? startBlockOffset : 0;
                    int to = i == endBlock ? endBlockOffset : MifareClassic.BLOCK_SIZE;
                    int j;
                    // Padd start with 0's
                    for(j = 0; j < from; j++)
                        block[j] = 0;
                    // Fill with data
                    for(j = from; j < to; j++)
                        block[j] = bytes[j + (i - startBlock) * MifareClassic.BLOCK_SIZE - startBlockOffset];
                    // Padd remaining with 0's
                    for(j = to; j < MifareClassic.BLOCK_SIZE; j++)
                        block[j] = 0;
                    technology.writeBlock(i, block);
                    i++;
                }
                endBlock++; // skipping last block in each sector to avoid overwriting settings
            }
        } finally {
            technology.close();
        }
    }

    @Override
    public void write(byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        // Convert address to block no and offset in block
        int startBlock, endBlock, startBlockOffset, endBlockOffset;
        startBlock = address / MifareClassic.BLOCK_SIZE;
        startBlockOffset = address - startBlock * MifareClassic.BLOCK_SIZE;
        endBlock = (address + bytes.length) / MifareClassic.BLOCK_SIZE;
        endBlockOffset = (address + bytes.length) % MifareClassic.BLOCK_SIZE;

        if(address < MIN_CONTIGUOUS_RW_ADDR || (address + bytes.length) > MAX_CONTIGUOUS_RW_ADDR) throw new IOException("Trying to access illegal address space");

        // Avoiding writing to last block in each sector -> need to move start and end block accordingly
        for(int i = 0; i < startBlock; i++) {
            int sector = mTech.blockToSector(i);
            int blocksInSector = mTech.getBlockCountInSector(sector);
            for(int j = 0; j < (blocksInSector - 1); j++) {
                i++;
                if(i == startBlock) break;
            }
            startBlock++;
            endBlock++;
        }

        // for each block, write 16 bytes
        byte[] block = new byte[16];
        for(int i = startBlock; i <= endBlock; i++) {
            // Authenticate sector
            int sector = mTech.blockToSector(i);
            if(!mTech.authenticateSectorWithKeyB(sector, MifareClassic.KEY_DEFAULT))
                throw new IOException("Unable to authenticate sector " + sector);

            for(int k = 0; k < (mTech.getBlockCountInSector(i) - 1); k++) {
                if(i > endBlock) break;

                // Copy bytes to block
                int from = i == startBlock ? startBlockOffset : 0;
                int to = i == endBlock ? endBlockOffset : MifareClassic.BLOCK_SIZE;
                int j;
                // Padd start with 0's
                for(j = 0; j < from; j++)
                    block[j] = 0;
                // Fill with data
                for(j = from; j < to; j++)
                    block[j] = bytes[j + (i - startBlock) * MifareClassic.BLOCK_SIZE - startBlockOffset];
                // Padd remaining with 0's
                for(j = to; j < MifareClassic.BLOCK_SIZE; j++)
                    block[j] = 0;
                mTech.writeBlock(i, block);
                i++;
            }
            endBlock++; // skipping last block in each sector to avoid overwriting settings
            startBlock++; // required to offset in bytes[] relative to address
        }

    }

    @Override
    public double timeRTT(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_MIFARE_CLASSIC)) throw new UnsupportedOperationException("Tag does not support MifareClassic");

        String uuid = UUID.randomUUID().toString();
        MifareClassic technology = (MifareClassic) tech;
        try {
            technology.connect();
            Utils.SimpleTimer.startTimer(uuid);
            technology.authenticateSectorWithKeyB(0, MifareClassic.KEY_DEFAULT);
            return Utils.SimpleTimer.getElapsedSeconds(uuid);
        } finally {
            Utils.SimpleTimer.removeTimer(uuid);
            technology.close();
        }
    }

    @Override
    public int getMinContiguousRWAddress() {
        return MIN_CONTIGUOUS_RW_ADDR;
    }

    @Override
    public int getMaxContiguousRWAddress() {
        return MAX_CONTIGUOUS_RW_ADDR;
    }

    @Override
    public byte[] transmitCommand(TagTechnology tech, byte[] bytes) throws IOException {
        if(tech == null) throw new NullPointerException("tech");
        if(! Arrays.asList(tech.getTag().getTechList()).contains(Identifiers.TECH_MIFARE_CLASSIC)) throw new UnsupportedOperationException("Tag does not support MifareClassic");

        MifareClassic technology = (MifareClassic) tech;
        try {
            technology.connect();
            // set max response time
            technology.setTimeout(500);
            return technology.transceive(bytes);
        } finally {
            technology.close();
        }
    }
    //endregion ITagTechnologyWrapper implementation
}
