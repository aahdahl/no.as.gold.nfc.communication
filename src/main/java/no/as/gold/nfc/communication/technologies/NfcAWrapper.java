package no.as.gold.nfc.communication.technologies;

import android.nfc.Tag;
import android.nfc.tech.NfcA;
import android.nfc.tech.TagTechnology;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import no.as.gold.nfc.communication.Utils.Identifiers;

/**
 * This class wraps around all implemented NfcA protocols.
 * Created by Aage Dahl on 25.02.14.
 */
public class NfcAWrapper implements ITagTechnologyWrapper {
    private static enum TagType {
        TAG1,
        TAG2,
        TAG4
    }

    ITagTechnologyWrapper mTech;
    public NfcAWrapper(Tag tag) {

        byte[] id = tag.getId();
        // Assert that this technology supports NfcA
        if(!Arrays.asList(tag.getTechList()).contains(Identifiers.TECH_NFC_A)) throw new UnsupportedOperationException("tag");

        switch (getTagType(tag)) {
            case TAG1:
                mTech = new Tag1NfcAWrapper();
                break;
            case TAG2:
                mTech = new Tag2NfcAWrapper();
                break;
            case TAG4:
                throw new UnsupportedOperationException("Tag type 4 not supported");
            default: throw new UnsupportedOperationException("tag");
        }
    }

    /**
     * gets the tag type associated with this tag
     * @param tag tag technology to be used
     * @return enum value of identified tag
     */
    private TagType getTagType(Tag tag) {

        // first 2 bytes of id contains H0 and H1 which identifies the id
        // read ID
        NfcA technology = NfcA.get(tag);
        byte []RID;
        try {
            technology.connect();
            RID = technology.transceive(new byte[] {0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00});
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                technology.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // TODO: improve identification method
        Collection<String> techList = Arrays.asList(tag.getTechList());
        if(techList.contains(Identifiers.TECH_ISO_DEP)) {
            return TagType.TAG4;
        } else if(techList.contains(Identifiers.TECH_MIFARE_ULTRALIGHT)) {
            return TagType.TAG2;
        } else if(techList.contains(Identifiers.TECH_NFC_A)) {
            return TagType.TAG1;
        }

        // Unknown
        return null;
    }

    @Override
    public void connect(TagTechnology tech) throws IOException {
        mTech.connect(tech);
    }

    @Override
    public void close() throws IOException {
        mTech.close();
    }

    @Override
    public byte[] read(int length, int address) throws UnsupportedOperationException, NullPointerException, IOException {
        return mTech.read(length, address);
    }

    @Override
    public byte[] read(TagTechnology tech, int length, int address) throws UnsupportedOperationException, NullPointerException, IOException {
        return mTech.read(tech,length,address);
    }

    @Override
    public byte[] readAll(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        return mTech.readAll(tech);
    }

    @Override
    public byte[] readAll() throws UnsupportedOperationException, NullPointerException, IOException {
        return mTech.readAll();
    }

    @Override
    public void write(TagTechnology tech, byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        mTech.write(tech,bytes,address);
    }

    @Override
    public void write(byte[] bytes, int address) throws UnsupportedOperationException, IndexOutOfBoundsException, NullPointerException, IOException {
        mTech.write(bytes, address);
    }

    @Override
    public double timeRTT(TagTechnology tech) throws UnsupportedOperationException, NullPointerException, IOException {
        return mTech.timeRTT(tech);
    }

    @Override
    public int getMinContiguousRWAddress() {
        return mTech.getMinContiguousRWAddress();
    }

    @Override
    public int getMaxContiguousRWAddress() {
        return mTech.getMaxContiguousRWAddress();
    }

    @Override
    public byte[] transmitCommand(TagTechnology tech, byte[] bytes) throws IOException {
        return mTech.transmitCommand(tech, bytes);
    }
}
