package no.as.gold.nfc.communication.Utils;

import java.util.UUID;

/**
 * Created by aage on 22.11.13.
 */
public class Identifiers {

    //region supported tag technologies
    public static final String TECH_NFC_A = "android.nfc.tech.NfcA";
    public static final String TECH_NFC_B = "android.nfc.tech.NfcB";
    public static final String TECH_NFC_F = "android.nfc.tech.NfcF";
    public static final String TECH_NFC_V = "android.nfc.tech.NfcV";
    public static final String TECH_ISO_DEP = "android.nfc.tech.IsoDep";
    public static final String TECH_NDEF = "android.nfc.tech.Ndef";
    public static final String TECH_MIFARE_CLASSIC = "android.nfc.tech.MifareClassic";
    public static final String TECH_MIFARE_ULTRALIGHT = "android.nfc.tech.MifareUltralight";
    //endregion supported tag technologies

    public static String WriterFragmentID = UUID.randomUUID().toString(); // "no.as.gold.nfc.WriterTagFragment";
    public static String ReaderFragmentID = UUID.randomUUID().toString(); // "no.as.gold.nfc.ReaderTagFragment";
    public static String BWriterFragmentID = UUID.randomUUID().toString(); // "no.as.gold.nfc.WriterTagFragment";
    public static String BReaderFragmentID = UUID.randomUUID().toString(); // "no.as.gold.nfc.ReaderTagFragment";
    public static String BReadBenchmarkReaderID = UUID.randomUUID().toString(); // "no.as.gold.nfc.BWriteBenchmarkFragment";
    public static String BReadBenchmarkWriterID = UUID.randomUUID().toString(); // "no.as.gold.nfc.BReadBenchmarkFragment";
    public static String BReadAllBenchmarkReaderID = UUID.randomUUID().toString(); // "no.as.gold.nfc.BenchmarkFragment";
    public static String BReadAllBenchmarkWriterID = UUID.randomUUID().toString(); // "no.as.gold.nfc.BenchmarkFragment";
    public static String BWriteBenchmarkReaderID = UUID.randomUUID().toString();
    public static String BWriteBenchmarkWriterID = UUID.randomUUID().toString();
    public static String NdefReadBenchmarkReaderID = UUID.randomUUID().toString(); // "no.as.gold.nfc.BenchmarkFragment";
    public static String NdefReadBenchmarkWriterID = UUID.randomUUID().toString(); // "no.as.gold.nfc.BenchmarkFragment";
    public static String NdefWriteBenchmarkReaderID = UUID.randomUUID().toString(); // "no.as.gold.nfc.BenchmarkFragment";
    public static String NdefWriteBenchmarkWriterID = UUID.randomUUID().toString(); // "no.as.gold.nfc.BenchmarkFragment";
}
